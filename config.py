import tensorflow as tf
import tensorflow_addons as tfa
import gctf
import os
from custom_layers import mish


# Phase parameters
using_phase = "shared"
assert using_phase in [False, "separate", "shared"], "using_phase must be one of False, 'shared' or 'separate'."

# Model hyperparameters
DIMS = (128, 128, 1)
DEC_DIMS = (4, 4, 256)
N_Z = 128
BATCH_SIZE = None

batchnorm = True
activation = tf.nn.leaky_relu
final_dec_activation = ["relu", tf.math.atan]
final_disc_activation = "tanh"

# Train hyperparameters
n_epochs = 1200
epoch_threshold = 100

# Optimizers
lr = 1e-4
if using_phase == "separate":
    # Adam optimiser
    gen_optimizer = [tf.keras.optimizers.Adam(lr, beta_1=0.5),
                     tf.keras.optimizers.Adam(lr, beta_1=0.5)]
    disc_optimizer = [tf.keras.optimizers.RMSprop(lr),
                      tf.keras.optimizers.RMSprop(lr)]
    # RectAdam optimiser
    # gen_mag_inner = tfa.optimizers.RectifiedAdam(learning_rate=lr)
    # gen_mag_inner.get_gradients = gctf.centralized_gradients_for_optimizer(gen_mag_inner)
    # gen_mag_optimizer = tfa.optimizers.Lookahead(gen_mag_inner)
    # disc_mag_inner = tfa.optimizers.RectifiedAdam(learning_rate=lr)
    # disc_mag_inner.get_gradients = gctf.centralized_gradients_for_optimizer(disc_mag_inner)
    # disc_mag_optimizer = tfa.optimizers.Lookahead(disc_mag_inner)
    # gen_phase_inner = tfa.optimizers.RectifiedAdam(learning_rate=lr)
    # gen_phase_inner.get_gradients = gctf.centralized_gradients_for_optimizer(gen_phase_inner)
    # gen_phase_optimizer = tfa.optimizers.Lookahead(gen_phase_inner)
    # disc_phase_inner = tfa.optimizers.RectifiedAdam(learning_rate=lr)
    # disc_phase_inner.get_gradients = gctf.centralized_gradients_for_optimizer(disc_phase_inner)
    # disc_phase_optimizer = tfa.optimizers.Lookahead(disc_phase_inner)
    # gen_optimizer = [gen_mag_optimizer,
    #                  gen_phase_optimizer]
    # disc_optimizer = [disc_mag_optimizer,
    #                   disc_phase_optimizer]

else:
    # Adam optimiser
    gen_optimizer = tf.keras.optimizers.Adam(lr, beta_1=0.5)
    disc_optimizer = tf.keras.optimizers.RMSprop(lr)
    # RectAdam optimiser
    # gen_inner = tfa.optimizers.RectifiedAdam(learning_rate=lr)
    # gen_inner.get_gradients = gctf.centralized_gradients_for_optimizer(gen_inner)
    # gen_optimizer = tfa.optimizers.Lookahead(gen_inner)
    # disc_inner = tfa.optimizers.RectifiedAdam(learning_rate=lr)
    # disc_inner.get_gradients = gctf.centralized_gradients_for_optimizer(disc_inner)
    # disc_optimizer = tfa.optimizers.Lookahead(disc_inner)


# Generator parameters
config_path = os.path.realpath(__file__)
if "Soline" in config_path:
    batch_size = 4
elif "killian" in config_path:
    batch_size = 64

sr = 48000
args = dict(
    batch_size=batch_size,
    wl=0.03,
    ovlp=80,
    window=tf.signal.hamming_window,
    n_mels=DIMS[0],
    n_times=DIMS[1],
    dtype="float32",
    audio_format="wav",
    labels_format="tsv",
    augment_function=None,
    n_channels=6,
    exclude=["Pls", "Inc"],
    duration=.75,
    phase=using_phase in ['separate', 'shared'],
    scale=False,
    norm=True,
    random_channel=True,
)
training_args = dict(
    negative_prop=0.,
    k=1
)
