import tensorflow as tf
import tensorflow_addons as tfa
import gctf
import os
from custom_layers import mish

using_phase=False
config_path = os.path.realpath(__file__)

# Model hyperparameters
DIMS = (128, 128, 1)
DEC_DIMS = (4, 4, 256)
N_Z = 128
BATCH_SIZE = None

batchnorm = True
activation = tf.nn.leaky_relu
final_dec_activation = "relu"
final_disc_activation = "tanh"

n_epochs = 2500
epoch_threshold = 100
lr = 1e-4
gen_optimizer = tf.keras.optimizers.Adam(lr, beta_1=0.5)
disc_optimizer = tf.keras.optimizers.RMSprop(lr)
# gen_inner = tfa.optimizers.RectifiedAdam(learning_rate=lr)
# gen_inner.get_gradients = gctf.centralized_gradients_for_optimizer(gen_inner)
# gen_optimizer = tfa.optimizers.Lookahead(gen_inner)
# disc_inner = tfa.optimizers.RectifiedAdam(learning_rate=lr)
# disc_inner.get_gradients = gctf.centralized_gradients_for_optimizer(disc_inner)
# disc_optimizer = tfa.optimizers.Lookahead(disc_inner)


# Generator parameters
if "Soline" in config_path:
    batch_size = 8
elif "killian" in config_path:
    batch_size = 64

sr = 48000
args = dict(
    batch_size=batch_size,
    wl=0.03,  # window length in seconds
    ovlp=80,  # overlap between successive windows
    window=tf.signal.hamming_window,
    n_mels=DIMS[0],  # number of mel coefficients in a frame of the spectrogram
    n_times=DIMS[1],
    dtype="float32",  # default
    audio_format="wav",  # default
    labels_format="tsv",  # default
    augment_function=None,
    n_channels=6,
    exclude=["Pls", "Inc"],
    duration=.75,  # durations in seconds
    phase=False,
    scale=False,
    norm=True,
)
training_args = dict(
    negative_prop=0.,
    k=1.
)
