import config
import train


def set_param(optimizer:str, activation:str, batchnorm:bool, lr=None): # lr = 'RectifiedAdam' or 'Adam'
    
    with open(config.config_path, 'r') as f:
        lines = f.readlines()

    new_config = []
    for line in lines:
        if line.startswith("lr = "):
            if lr != None:
                 new_config.append(f"lr = {lr}\n")
            elif optimizer == "RectifiedAdam":
                new_config.append("lr = 10**(-2.5)\n")
            else:
                new_config.append(line)
        elif "optimizer" in line:
            if optimizer == "Adam":
                if line.startswith("#"):
                    new_config.append(line.replace("# ", ""))
                else:
                    new_config.append("# " + line)
            else:
                new_config.append(line)
        elif line.startswith("activation = "):
            new_config.append(f"activation = {activation}\n")
        elif line.startswith("batchnorm = "):
            new_config.append(f"batchnorm = {batchnorm}\n")
        else:
            new_config.append(line)
    
    with open(config.config_path, 'w') as f:
        f.writelines(new_config)


# set_param(optimizer:str, activation:str, batchnorm:bool, lr=None)

set_param(optimizer="RectifiedAdam", activation="tf.nn.leaky_relu", batchnorm=False)
train

set_param(optimizer="RectifiedAdam", activation="mish", batchnorm=False)
train

set_param(optimizer="RectifiedAdam", activation="mish", batchnorm=True)
train
