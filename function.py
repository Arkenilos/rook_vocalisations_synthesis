import tensorflow as tf
import numpy as np
import pandas as pd
from itertools import chain


def flatten(l):
    """
    Flattens a list of lists into a list.
    """
    return list(chain.from_iterable(l))


def model_size(model, batch_input_shape):
    model.build(input_shape = batch_input_shape[1:])
    types = [int(l.dtype[-2:]) // 8 for l in model.layers]
    weights_size = sum([tf.size(w).numpy() * t for w, t in zip(model.weights, types)])
    layer_size = [l.output_shape for l in model.layers]
    layer_size[0] = model.input_shape
    layer_size = [[v if v is not None else batch_input_shape[i] for i, v in enumerate(_)] for _ in layer_size]
    L = [np.prod(l) * t / 1024 for l, t in zip(layer_size, types)]

    return f"Model size: {round(sum(L) + weights_size / 1024)} KB"


def merge_intervals(intervals):
    """
    Mostly a helper function
    :param intervals: list of tuples of the shape (left, right)
    :return: list of tuples corresponding to the unions of the input intervals
    """
    result = []
    (start_candidate, stop_candidate) = intervals[0]
    append_method = result.append
    for (start, stop) in intervals[1:]:
        if start <= stop_candidate:
            stop_candidate = max(stop, stop_candidate)
        else:
            append_method((start_candidate, stop_candidate))
            (start_candidate, stop_candidate) = (start, stop)
    append_method((start_candidate, stop_candidate))
    return result


def outer(intervals):
    """
    Helper function for interval union
    """
    def inner(x):
        res = None
        for g, l in enumerate(intervals):
            if any([_ in l for _ in x]):
                res = g
                break
        return res
    return inner


def interval_union(df, col1, col2, inplace=True):
    if inplace:
        df.sort_values(col1, inplace=True)
    else:
        df = df.sort_values(col1, inplace=False)

    # Get intervals and then their union for the two columns
    intervals = pd.IntervalIndex.from_arrays(left=df[col1], right=df[col2], closed="both")
    intervals = intervals.to_tuples()
    intervals = merge_intervals(intervals)
    intervals = pd.IntervalIndex.from_tuples(intervals, closed="both")

    inner = outer(intervals)
    df["group"] = df[[col1, col2]].apply(inner, axis=1)

    df = df\
        .groupby("group", as_index=False)\
        .agg({col1: "min", col2: "max"})\
        .drop("group", axis=1)
    return df


def overlapping_lines(lines):
    ovlp = []
    for i in range(lines.shape[0]-1):
        j = i+1
        while j < lines.shape[0] and lines['name'].iloc[i] == lines['name'].iloc[j] and float(lines.iloc[i].end) > float(lines.iloc[j].start):
            ovlp.append(i)
            ovlp.append(j)
            j += 1
    return ovlp


def to_keep(lines, sources_to_del, events_to_del, max_duration):
    ovlp = overlapping_lines(lines)
    lines = lines.drop(list(ovlp), axis=0)
    lines = lines.loc[~lines['source'].isin(sources_to_del)]
    lines = lines.loc[~lines['event'].str.contains('|'.join(events_to_del), na=True)]
    lines = lines.loc[lines['end'] - lines['start'] <= max_duration]
    lines = lines.dropna(subset=['channel'])
    return lines.reset_index()
