import tensorflow as tf
import tensorflow_probability as tfp
import soundfile as sf
import numpy as np
import os
import importlib.util
import glob
import shutil
import librosa
import torch
import torchaudio
import config
from network import *
from audio_generator import AudioGenerator


### General functions


def mel_to_stft_librosa(mels, wl, mel_compr=True, sr=48000):
    if mel_compr:
        mels = tf.pow(mels, 3.)
    mels = tf.squeeze(mels)
    stfts = [librosa.feature.inverse.mel_to_stft(m.numpy(),
                                                 sr, n_fft=wl,
                                                 power=1.0,
                                                 htk=True) for m in mels]
    return stfts


def mel_to_stft_Sainburg(mels, wl, mel_compr=True, sr=48000, n_mels=128):
    mel_filters = librosa.filters.mel(sr, n_fft=wl, n_mels=n_mels, htk=True)
    with np.errstate(divide="ignore", invalid="ignore"):
        mel_inversion_filters = tf.constant(np.nan_to_num(np.divide(mel_filters, np.sum(mel_filters, axis=0))).T)
    if mel_compr:
        mels = tf.pow(mels, 3.)
    mels = tf.squeeze(mels)
    stfts = tf.matmul(mel_inversion_filters, mels)
    return stfts


def mel_to_stft_pytorch(mels, wl, mel_compr=True, sr=48000, n_mels=128):
    if mel_compr:
        mels = tf.pow(mels, 3.)
    mels = torch.from_numpy(mels.numpy())
    mels = torch.squeeze(mels)
    stfts = [torchaudio.transforms.InverseMelScale(n_stft=wl, 
                                                   n_mels=n_mels,
                                                   sample_rate=sr,
                                                   f_min=0.0,
                                                   f_max=None,
                                                   max_iter=10000,
                                                   tolerance_loss=1e-05,
                                                   tolerance_change=1e-08,
                                                   sgdargs=None,
                                                   norm='slaney')(mel) for mel in mels]
    return stfts


def stft_to_wav_GL(stfts, step, wl):
    if "numpy" in dir(stfts[0]):
        stfts = [s.numpy() for s in stfts]
    wavs = [librosa.griffinlim(s,
                               hop_length=step,
                               win_length=wl,
                               window="hamming",
                               center=False,
                               pad_mode="constant") for s in stfts]

    return wavs


def stft_to_wav_with_phase(stfts, step, wl):
    try:
        stfts = tf.squeeze(stfts)
    except tf.errors.InvalidArgumentError:
        pass
    mags, phases = zip(*[tf.unstack(s, axis=-1) for s in stfts])
    mags = [tf.transpose(m) for m in mags]
    phases = [tf.transpose(p) for p in phases]
    stfts = [tf.complex(m*tf.math.cos(p), m*tf.math.sin(p)) for m, p in zip(mags, phases)]
    wavs = [tf.signal.inverse_stft(s,
                                   frame_length=wl,
                                   frame_step=step,
                                   fft_length=wl,
                                   window_fn=tf.signal.inverse_stft_window_fn(
                                              frame_step=step,
                                              forward_window_fn=tf.signal.hamming_window
                                             ),
                                   name=None
                                  ) for s in stfts]
    return wavs


def write_audio(model_name, wavs, origin:str, sr=48000):
    os.makedirs(f"wav/{model_name}/{origin}", exist_ok=True)
    for wav in wavs:
        if glob.glob(f"wav/{model_name}/{origin}/*.wav") == []:
            i = 0
        else:
            i = int(glob.glob(f"wav/{model_name}/{origin}/*.wav")[-1][-8:-4]) + 1
        print(f"{origin}, wav n°{i:05d}")
        sf.write(f"wav/{model_name}/{origin}/{i:05d}.wav", data=wav, samplerate=sr)


def move_and_rename(model_name, directory:str):
    if not os.path.exists(f"wav/{model_name}/{directory}"):
        raise ValueError(f"move_and_rename: directory does not exist in wav/{model_name}")   
    os.makedirs(f"wav/{model_name}/{directory}/All", exist_ok=False)
    subdirs = os.listdir(f"wav/{model_name}/{directory}")
    for subdir in subdirs:
        for file in glob.glob(f"wav/{model_name}/{directory}/{subdir}/*.wav"):
            new_name = f"{file[-8:-4]}" + "_" + f"{subdir}" + ".wav"
            shutil.copyfile(file, f"wav/{model_name}/{directory}/All/{new_name}")


### Qualitative comparisons of GAIA outputs


def GAIA_output_to_audio(model_name, model, example_data, step, wl, x=False, x_gen=False, x_interp=False, phase=False):
    if sum([x, x_gen, x_interp])>1:
        raise ValueError("mel_to_audio: only 1 arg can be True")
    _, xg, _, xi, _, _, _ = model(example_data)
    if x:
        mod = "x"
        mels = example_data
    elif x_gen:
        mod = "xg"
        mels = xg
    elif x_interp:
        mod = "xi"
        mels = xi

    if phase:
        mels, _ = zip(*[tf.unstack(s, axis=-1) for s in tf.squeeze(mels)])
        mels = tf.stack(mels, axis=0)

    stfts = mel_to_stft_librosa(mels, wl)
    stfts = tf.convert_to_tensor(stfts)
    wavs = stft_to_wav_GL(stfts, step, wl)
    write_audio(model_name, wavs, f"GAIA_outputs/{mod}")


def write_audio_GAIA_outputs(test_dataset, model_name, model, step, wl, nb_batch:int=None, phase=False):
    if nb_batch is None:
        nb_batch=test_dataset.__len__()
    for index in range(nb_batch):
        _, _, _, _, mels, _, _, _ = test_dataset.magic(index)
        GAIA_output_to_audio(model_name, model, mels, step, wl, x=True, phase=phase)
        GAIA_output_to_audio(model_name, model, mels, step, wl, x_gen=True, phase=phase)
        GAIA_output_to_audio(model_name, model, mels, step, wl, x_interp=True, phase=phase)


### Qualitative comparisons of audio reconstruction steps


def stft2wav_GriffinLim_test(model_name, stfts, step, wl):
    wavs = stft_to_wav_GL(stfts, step, wl)
    write_audio(model_name, wavs, "Audio_reconstruction_steps/Griffin-Lim_test")


def stft2wav_using_phase_test(model_name, stfts, step, wl):
    wavs = stft_to_wav_with_phase(stfts, step, wl)
    write_audio(model_name, wavs, "Audio_reconstruction_steps/Phase_test")


def mel2stft_librosa_test(model_name, mels, step, wl):
    stfts = mel_to_stft_librosa(mels, wl)
    wavs = stft_to_wav_GL(stfts, step, wl)
    write_audio(model_name, wavs, "Audio_reconstruction_steps/Mel-to-Stft_Librosa_test")


def mel2stft_Sainburg_test(model_name, mels, spectrograms, step, wl):
    stfts = mel_to_stft_Sainburg(mels, wl, spectrograms)
    wavs = stft_to_wav_GL(stfts, step, wl)
    write_audio(model_name, wavs, "Audio_reconstruction_steps/Mel-to-Stft_Sainburg_test")


def GAIA_test(model_name, model, example_data, step, wl, phase=False):
    _, xg, _, _, _, _, _ = model(example_data)  # z, xg, zi, xi, d_xi, d_x, d_xg
    # stft = mel_to_stft_Sainburg(xg, wl)
    if phase:
        mags, phases = tf.unstack(xg, axis=-2)
        stft_mags = mel_to_stft_librosa(mags, wl, mel_compr=True, sr=48000)
        stft_phases = mel_to_stft_librosa(phases, wl, mel_compr=True, sr=48000)
        stfts = tf.stack([stft_mags, stft_phases], axis=-1)
        wav = stft_to_wav_with_phase(stfts, step, wl)
    else:
        stft = mel_to_stft_librosa(xg, wl)
        wav = stft_to_wav_GL(stft, step, wl)
    write_audio(model_name, wav, "Audio_reconstruction_steps/GAIA_test")


def write_audio_reconstruction_steps(test_dataset, model_name, model, step, wl, phase=False):
    for index in range(2):
        _, wavs, spectrograms, _, chunks, _, _, _ = test_dataset.magic(index)  # label_df, wavs, spectrograms, chunks_un, chunks, y_chunks, y_binary, masks

        GAIA_test(model_name, model, chunks, step, wl, phase=phase)

        if phase:
            # Get mel_mags (chunks)
            chunks, mel_phases = zip(*[tf.unstack(s, axis=-1) for s in tf.squeeze(chunks)])
            chunks = tf.stack(chunks, axis=0)
            mel_phases = tf.stack(mel_phases, axis=0)
            # Write audio with phase
            stft2wav_using_phase_test(model_name, spectrograms, step, wl)
            # Get stft_mags (spectrograms)
            spectrograms, _ = zip(*[tf.unstack(s, axis=-1) for s in spectrograms])
            
        stft2wav_GriffinLim_test(model_name, spectrograms, step, wl)
        mel2stft_librosa_test(model_name, chunks, step, wl)
        mel2stft_Sainburg_test(model_name, chunks, spectrograms, step, wl)
        write_audio(model_name, wavs, "Audio_reconstruction_steps/Origin")


def compare_stft2wav_methods_audio(test_dataset, step, wl):
    for index in range(3):
        _, _, spectrograms, _, chunks, _, _, _ = test_dataset.magic(index)
        _, chunks, _, _, _, _, _ = model(chunks)
        # With GL
        mags, phases = zip(*[tf.unstack(s, axis=-1) for s in tf.squeeze(chunks)])
        mags = tf.stack(mags, axis=0)
        phases = tf.stack(phases, axis=0)
        stft_mags = mel_to_stft_librosa(mags, wl, mel_compr=True, sr=48000)
        stft_phases = mel_to_stft_librosa(phases, wl, mel_compr=True, sr=48000)
        wavs_GL = stft_to_wav_GL(stft_mags, step, wl)
        write_audio(model_name, wavs_GL, "Audio_reconstruction_steps/Phase_test/WithGL", sr=48000)

        # With phase
        spec_mags, spec_phases = zip(*[tf.unstack(s, axis=-1) for s in spectrograms])

        spec_mags_shape = [_.shape[1] for _ in spec_mags]
        stft_mags_shape = [_.shape[1] for _ in stft_mags]
        spec_mags = [np.pad(spec_mags[i],
                     pad_width=((0,0), [0, stft_mags_shape[i] - spec_mags_shape[i]]),
                     constant_values=0) for i in range(len(spec_mags_shape))]
        spec_phases_shape = [_.shape[1] for _ in spec_phases]
        stft_phases_shape = [_.shape[1] for _ in stft_phases]
        spec_phases = [np.pad(spec_phases[i],
                     pad_width=((0,0), [0, stft_phases_shape[i] - spec_phases_shape[i]]),
                     constant_values=0) for i in range(len(spec_phases_shape))]

        stfts = tf.stack([stft_mags, stft_phases], axis=-1)
        stfts2 = tf.stack([stft_mags, spec_phases], axis=-1)
        wavs_res = stft_to_wav_with_phase(stfts, step, wl)
        wavs_ori = stft_to_wav_with_phase(stfts2, step, wl)
        write_audio(model_name, wavs_res, "Audio_reconstruction_steps/Phase_test/WithPhaseReseau", sr=48000)
        write_audio(model_name, wavs_ori, "Audio_reconstruction_steps/Phase_test/WithPhaseOrigin", sr=48000)
    move_and_rename(model_name, "Audio_reconstruction_steps/Phase_test")


### Quantitative comparisons of audio reconstruction steps


def get_matrix_reconstruction_steps(model, origin_wav, origin_stft, origin_mel, origin_y_mel, step, wl, phase=False, eval='mag'):
    _, reconstruct_mel, _, _, _, _, _ = model(origin_mel)  # z, xg, zi, xi, d_xi, d_x, d_xg
    if phase:
        origin_mel, origin_mel = tf.unstack(origin_mel, axis=-2)
        if eval=='mag':
            origin_y_mel, _ = tf.unstack(origin_y_mel, axis=-2)
            origin_stft, _ = zip(*[tf.unstack(_, axis=-1) for _ in origin_stft])
            reconstruct_mel, _ = tf.unstack(reconstruct_mel, axis=-2)
        elif eval=='phase':
            _, origin_y_mel = tf.unstack(origin_y_mel, axis=-2)
            _, origin_stft = zip(*[tf.unstack(_, axis=-1) for _ in origin_stft])
            _, reconstruct_mel = tf.unstack(reconstruct_mel, axis=-2)
        else:
            raise ValueError("get_matrix_reconstruction_steps(): eval should be one of 'mag' or 'phase'" )
    reconstruct_wav = stft_to_wav_GL(origin_stft, step, wl)
    # reconstruct_stft = mel_to_stft_Sainburg(origin_mel, wl)
    reconstruct_stft = mel_to_stft_librosa(origin_mel, wl)

    return origin_wav, reconstruct_wav, origin_stft, reconstruct_stft, origin_y_mel, reconstruct_mel


def substact_matrix(origin, reconstruct, kind:str):
    if kind not in ["mel", "stft", "wav"]:
        raise ValueError("compare_matrix: kind should be 'mel', 'stft' or 'wav'")
    if kind == "stft":
        R_shape = [_.shape[1] for _ in reconstruct]
        O_shape = [_.shape[1] for _ in origin]
        origin = [np.pad(origin[i],
                        pad_width=((0,0), [0, R_shape[i] - O_shape[i]]),
                        constant_values=0) for i in range(len(O_shape))]
    if kind == "wav":
        substact = ( [(i - j)**2 for i, j in zip(origin, reconstruct)]) 
    else:
        substact = [(i - j)**2 for i, j in zip(origin, reconstruct)]
    return substact


def compare_stft2wav_methods_quanti(test_dataset, step, wl):
    withphase_reconstruct = [] ; GL_reconstruct = [] ; origin = []

    for index in range(test_dataset.__len__()):
        _, wavs, spectrograms, _, _, _, _, _ = test_dataset.magic(index)  # label_df, wavs, spectrograms, chunks_un, chunks, y_chunks, y_binary, masks
        print(f"Batch {index}")

        origin.extend(wavs)
        origin_shape = [w.shape[0] for w in wavs]

        withphase_reconstruct_wavs = stft_to_wav_with_phase(spectrograms, step, wl)
        withphase_reconstruct_wavs = [ t[:w] for t,w in zip(withphase_reconstruct_wavs, origin_shape)]
        withphase_reconstruct.extend(withphase_reconstruct_wavs)

        mag_spectrograms, _ = zip(*[tf.unstack(s, axis=-1) for s in spectrograms])
        GL_reconstruct_wavs = stft_to_wav_GL(mag_spectrograms, step, wl)
        GL_reconstruct_wavs = [ t[:w] for t,w in zip(GL_reconstruct_wavs, origin_shape)]
        GL_reconstruct.extend(GL_reconstruct_wavs)

    reconstruct_list = [withphase_reconstruct, GL_reconstruct]
    reconstruct_type = ["Using interpolate phase", "Griffin-Lim"]
    for i, reconstruct in enumerate(reconstruct_list):
        # plt.plot(np.abs(origin[0]))
        # plt.plot(np.abs(reconstruct[0]), alpha=.5)
        # plt.show()
        origins = np.concatenate(origin)
        reconstruct = np.concatenate(reconstruct)
        comp = substact_matrix(origins, reconstruct, kind='wav')
        mean = np.mean(comp) ; var = np.var(comp) ; std = np.std(comp)
        cor = tfp.stats.correlation(origins, reconstruct, sample_axis=None, event_axis=None).numpy()
        print(f"{reconstruct_type[i]} reconstruction : m={mean}, var={var}, ec={std}, cor={cor}")


def assess_GL_phase(test_dataset):
    real=[] ; gl=[]
    for index in range(test_dataset.__len__()):
        _, _, spectrograms, _, _, _, _, _ = test_dataset.magic(index)
        print(f"Batch {index}")

        real_mags, REAL_phases = zip(*[tf.unstack(s, axis=-1) for s in spectrograms])
        GL_wavs = stft_to_wav_GL(real_mags, step, wl)
        GL_stfts = [ tf.signal.stft(w,
                           frame_length=wl,
                           fft_length=wl,
                           frame_step=step,
                           window_fn=tf.signal.hamming_window,
                           pad_end=True) for w in GL_wavs ]
        GL_phases = [tf.transpose(tf.math.angle(s)) for s in GL_stfts]

        REAL_phases = [(s - np.mean(s)) / np.std(s) for s in REAL_phases]
        GL_phases = [(s - np.mean(s)) / np.std(s) for s in GL_phases]

        real.extend(REAL_phases)
        gl.extend(GL_phases)

    real_shape = [_.shape[1] for _ in real]
    gl_shape = [_.shape[1] for _ in gl]
    real = [np.pad(real[i],
                     pad_width=((0,0), [0, gl_shape[i] - real_shape[i]]),
                     constant_values=0) for i in range(len(real_shape))]
    
    real = np.concatenate([np.reshape(_, -1) for _ in real])
    gl = np.concatenate([np.reshape(_, -1) for _ in gl])
    comp = (real - gl)**2   
    
    mean = np.mean(comp) ; var = np.var(comp) ; std = np.std(comp)
    cor = tfp.stats.correlation(real, gl, sample_axis=None, event_axis=None).numpy()
    print(f"m={mean}, var={var}, ec={std}, cor={cor}")


def compare_mel2Stft_methods_quanti(test_dataset, wl, mel_compr:bool):
    Lib_reconstruct = [] ; TS_reconstruct = [] ; Torch_reconstruct = [] ; origin = []

    for index in range(test_dataset.__len__()):
        _, _, spectrograms, chunks_un, chunks, _, _, _ = test_dataset.magic(index)
        print(f"Batch {index}")

        Lib_reconstruct_stft = mel_to_stft_librosa(chunks, wl, chunks_un, mel_compr=mel_compr)
        TS_reconstruct_stft = mel_to_stft_Sainburg(chunks, wl, chunks_un, mel_compr=mel_compr)
        Torch_reconstruct_stft = mel_to_stft_pytorch(chunks, wl, chunks_un, mel_compr=mel_compr)
        Torch_reconstruct_stft = [ t[:721].numpy() for t in Torch_reconstruct_stft ]

        Lib_reconstruct_stft = [(s - np.mean(s)) / np.std(s) for s in Lib_reconstruct_stft]
        TS_reconstruct_stft = [(s - np.mean(s)) / np.std(s) for s in TS_reconstruct_stft]
        Torch_reconstruct_stft = [(s - np.mean(s)) / np.std(s) for s in Torch_reconstruct_stft]

        origin_stft = [(s - np.mean(s)) / np.std(s) for s in spectrograms]
        spec_shape = [_.shape[1] for _ in origin_stft]
        origin_stft = [tf.pad(origin_stft[i],
                               paddings=((0,0), [0, 128 - spec_shape[i]]),
                               constant_values=0) for i in range(len(spec_shape))]

        Lib_reconstruct.append(Lib_reconstruct_stft)
        TS_reconstruct.append(TS_reconstruct_stft)
        Torch_reconstruct.append(Torch_reconstruct_stft)
        origin.append(origin_stft)

    origin = np.concatenate(origin)
    reconstruct_list = [Lib_reconstruct, TS_reconstruct, Torch_reconstruct]
    reconstruct_type = ["Librosa", "Sainburg", "Torch"]

    for i, reconstruct in enumerate(reconstruct_list):
        reconstruct = np.concatenate(reconstruct)
        comp = substact_matrix(origin, reconstruct, kind='stft')
        mean = np.mean(comp) ; var = np.var(comp) ; std = np.std(comp)
        cor = tfp.stats.correlation(tf.reshape(origin, -1), tf.reshape(reconstruct, -1), sample_axis=0, event_axis=None).numpy()
        print(f"{reconstruct_type[i]} reconstruction : m={mean}, var={var}, ec={std}, cor={cor}")


if __name__ == "__main__":

    # model(s) path(s)
    if "Soline" in config.config_path:
        os.chdir("C:/Users/Faulst/Documents/Soline/Code/database_rooks")
    elif "killian" in config.config_path:
        os.chdir("C:/Users/killian/Desktop/database_rooks")
    # pass a list of paths to get multiple models
    models = "runs/GAIA"

    # obtain data
    path = "data/test"
    data = os.listdir(path)

    # Retrieve model
    model_name = "20210617_101749"
    model_path = f"{models}/{model_name}"
    weights_path = glob.glob(f"{model_path}/*.h5")

    # Retrieve config
    spec = importlib.util.spec_from_file_location("config", f'{model_path}/config.py')
    config = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(config)

    # Generator
    config.args.update({"batch_size": 8})
    config.args.update({"norm": True})
    config.args.update({"scale": False})
    config.args.update({"random_channel": False})
    config.args.update({"test": False})

    test_dataset = AudioGenerator(data=path,
                                **config.args,
                                )
    # Interpolate function
    if model_name == '20210608_001004' or model_name == '20210611_180553_VEAGAN':
        interpolate_z = interpolate_z_VAE
    else:
        interpolate_z = interpolate_z_GAIA

    # model
    if hasattr(config, "using_phase") and config.using_phase == "shared":
        gaia_kwargs = dict(
            enc_input_mag = tf.keras.layers.Input(shape=config.DIMS),
            enc_input_phase = tf.keras.layers.Input(shape=config.DIMS),
            dec_out_mag = tf.keras.layers.Conv2DTranspose(filters=1, kernel_size=3, strides=(1, 1), padding="SAME", activation=config.final_dec_activation[0]),
            dec_out_phase = tf.keras.layers.Conv2DTranspose(filters=1, kernel_size=3, strides=(1, 1), padding="SAME", activation=config.final_dec_activation[1]),
            decDIMS=config.DEC_DIMS,
            N_Z = config.N_Z,
            activation=config.activation,
            batchnorm=config.batchnorm,
            final_activation=config.final_dec_activation
        )
    else:
        gaia_kwargs = dict(
            enc=encoder,
            encDIMS=config.DIMS,
            N_Z=config.N_Z,
            BATCH_SIZE=config.BATCH_SIZE,
            activation=config.activation,
            batchnorm=config.batchnorm,
            dec=decoder,
            decDIMS=config.DEC_DIMS,
            final_activation=config.final_dec_activation
        )
    model = GAIA(
        using_phase=config.using_phase if hasattr(config, "using_phase") else False,
        **gaia_kwargs,
        unet_list=unet_function(config.DIMS,
                                config.BATCH_SIZE,
                                activation=config.activation,
                                final_activation=config.final_disc_activation),
        gen_optimizer=config.gen_optimizer,
        disc_optimizer=config.disc_optimizer,
        interpolate_function = interpolate_z,
        interpolate_kwargs=dict(chsq=Chi2(df=1 / config.args["batch_size"]))
    )

    # Model initialisation and loading
    model(test_dataset.__getitem__(0)[0])
    model.load_weights(weights_path[-1])

    # Args
    sr = config.sr
    wl = int(config.args['wl'] * sr)
    ovlp = config.args['ovlp'] if 0. < config.args['ovlp'] < 1. else config.args['ovlp'] / 100
    step = int(wl * (1-ovlp))

    
    # write_audio_reconstruction_steps(test_dataset, model_name, model, step, wl, phase=config.args['phase'])
    # move_and_rename(model_name, "Audio_reconstruction_steps")
    # write_audio_GAIA_outputs(test_dataset, model_name, model, step, wl, nb_batch=2, phase=config.args['phase']) # COMMENT line 'config.args.update({"random_channel": True})'
    # move_and_rename(model_name, "GAIA_outputs")
    # compare_mel2Stft_methods_quanti(test_dataset, wl, mel_compr=True)
    # compare_stft2wav_methods_quanti(test_dataset, step, wl) # UNCOMMENT line 'config.args.update({"phase": True})'
