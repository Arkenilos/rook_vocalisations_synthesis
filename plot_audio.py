import os
import glob
import importlib.util
import soundfile as sf
import numpy as np
import librosa.display as disp
import matplotlib.pyplot as plt
from audio_generator import AudioGenerator
import config
from network import *


# model(s) path(s)
if "Soline" in config.config_path:
    os.chdir("C:/Users/cog/Documents/Soline/Code/database_rooks")
elif "killian" in config.config_path:
    os.chdir("C:/Users/killian/Desktop/database_rooks")
# pass a list of paths to get multiple models
models = "runs/GAIA"

# obtain data
path = "data/test"
data = os.listdir(path)

# Retrieve model
model_name = "20210527_191236"
model_path = f"{models}/{model_name}"
weights_path = glob.glob(f"{model_path}/*.h5")

# Retrieve config
spec = importlib.util.spec_from_file_location("config", f'{model_path}/config.py')
config = importlib.util.module_from_spec(spec)
spec.loader.exec_module(config)

# Generator
config.args.update({"batch_size": 8})  

test_dataset = AudioGenerator(data=path,
                              **config.args,
                              )

# Args
sr = config.sr
wl = int(config.args['wl'] * sr)
ovlp = config.args['ovlp'] if 0. < config.args['ovlp'] < 1. else config.args['ovlp'] / 100
step = int(wl * (1-ovlp))
n_mels=128
fmin=0
fmax = min(np.Inf, sr // 2)


_, wavs, spectrograms, chunks_un, chunks, _, _, _ = test_dataset.magic(0)  # label_df, wavs, spectrograms, chunks_un, chunks, y_chunks, y_binary, masks
for i in range(1):
    sf.write(f"C:/Users/cog/Documents/Soline/PhotosEmelineBraccini/Spectro/{i}.wav", wavs[i], samplerate=48000)
    fig, axs = plt.subplots(ncols=3, figsize=(18,5))
    # plt.rcParams.update({'font.size': 27})

    axs[0].axis([0, wavs[i].shape[0] / 48000, -.5, .5])
    cax0 = axs[0].plot([_ / 48000 for _ in range(wavs[i].shape[0])], wavs[i], c = "purple")
    axs[0].set_xlabel("Temps (s)", size=12)
    axs[0].set_ylabel("Amplitude (ua)", size=12)
    
    cax1 = disp.specshow((spectrograms[i]**(1/3)).numpy(), sr=sr, hop_length=step, ax=axs[1], x_axis="time", y_axis="linear")
    axs[1].set_xlabel("Temps (s)", size=12)
    axs[1].set_ylabel("Fréquence (Hz)", size=12)
    # cax1 = axs[1].matshow(spectrograms[i]**(1/3), aspect='auto', origin='lower') # **(1/3)
    
    cax2 = disp.specshow((chunks_un[i][:,:spectrograms[i].shape[1], 0]**(1/3)).numpy(), sr=sr, hop_length=step, ax=axs[2], x_axis="time", y_axis="mel")
    # cax2 = axs[2].matshow(chunks_un[i][:,:spectrograms[i].shape[1]]**(1/3), aspect='auto', origin='lower') # **(1/3)
    axs[2].set_xlabel("Temps (s)", size=12)
    axs[2].set_ylabel("Fréquence (Hz)", size=12)

    fig.colorbar(cax2, ax=axs[2]).ax.tick_params(labelsize=11)
    fig.colorbar(cax2, ax=axs[1]).ax.tick_params(labelsize=11)
    fig.colorbar(cax2, ax=axs[0]).ax.tick_params(labelsize=11)
    plt.axis('auto')

    [ax.tick_params(labelsize=11) for ax in axs]
    axs[0].set_title('Forme d\'onde', size=16)
    axs[1].set_title('Spectrogramme', size=16)
    axs[2].set_title('Mel spectrogramme', size=16)
    plt.show()
