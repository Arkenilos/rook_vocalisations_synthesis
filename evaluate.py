from copy import Error
import os
import glob
import importlib.util
import csv
import numpy as np
import tensorflow_probability as tfp
from audio_generator import AudioGenerator
from network import *
from mel_to_audio import get_matrix_reconstruction_steps, substact_matrix,\
                        write_audio_GAIA_outputs, move_and_rename


# Model(s) path(s)
if "Soline" in os.path.realpath(__file__):
    os.chdir("C:/Users/Faulst/Documents/Soline/Code/database_rooks")
elif "killian" in os.path.realpath(__file__):
    os.chdir("C:/Users/killian/Desktop/database_rooks")
# Pass a list of paths to get multiple models
models = "runs/GAIA"

# Obtain data
path = "data/test"
data = os.listdir(path)

# Retrieve model
model_name = "20210617_101749"
model_path = f"{models}/{model_name}"
weights_path = glob.glob(f"{model_path}/*.h5")

# Retrieve config
spec = importlib.util.spec_from_file_location("config", f"{model_path}/config.py")
config = importlib.util.module_from_spec(spec)
spec.loader.exec_module(config)

# Generator
if "Soline" in os.path.realpath(__file__):
    config.args.update({"batch_size": 8})

config.args.update({"norm": True})
config.args.update({"scale": False})
config.args.update({"random_channel": False})
config.args.update({"test": False})

test_dataset = AudioGenerator(data=path,
                              **config.args,
                              )

# Interpolate function
if model_name == '20210608_001004' or model_name == '20210611_180553':
    interpolate_z = interpolate_z_VAE
else:
    interpolate_z = interpolate_z_GAIA

# Model
if hasattr(config, "using_phase") and config.using_phase == "shared":
    gaia_kwargs = dict(
        enc_input_mag = tf.keras.layers.Input(shape=config.DIMS),
        enc_input_phase = tf.keras.layers.Input(shape=config.DIMS),
        dec_out_mag = tf.keras.layers.Conv2DTranspose(filters=1, kernel_size=3, strides=(1, 1), padding="SAME", activation=config.final_dec_activation[0]),
        dec_out_phase = tf.keras.layers.Conv2DTranspose(filters=1, kernel_size=3, strides=(1, 1), padding="SAME", activation=config.final_dec_activation[1]),
        decDIMS=config.DEC_DIMS,
        N_Z = config.N_Z,
        activation=config.activation,
        batchnorm=config.batchnorm,
        final_activation=config.final_dec_activation
    )
else:
    gaia_kwargs = dict(
        enc=encoder,
        encDIMS=config.DIMS,
        N_Z=config.N_Z,
        BATCH_SIZE=config.BATCH_SIZE,
        activation=config.activation,
        batchnorm=config.batchnorm,
        dec=decoder,
        decDIMS=config.DEC_DIMS,
        final_activation=config.final_dec_activation
    )

model = GAIA(
    using_phase=config.using_phase if hasattr(config, "using_phase") else False,
    **gaia_kwargs,
    unet_list=unet_function(config.DIMS,
                            config.BATCH_SIZE,
                            activation=config.activation,
                            final_activation=config.final_disc_activation),
    gen_optimizer=config.gen_optimizer,
    disc_optimizer=config.disc_optimizer,
    interpolate_function = interpolate_z,
    interpolate_kwargs=dict(chsq=Chi2(df=1 / config.args["batch_size"]))
)


# Model initialisation and loading
model(test_dataset.__getitem__(0)[0])
model.load_weights(weights_path[-1])


# Args
sr = config.sr
wl = int(config.args['wl'] * sr)
ovlp = config.args['ovlp'] if 0. < config.args['ovlp'] < 1. else config.args['ovlp'] / 100
step = int(wl * (1-ovlp))


# Evaluation modality
mod = "visual"
eval = "mag"
assert mod in ["quanti", "visual", "audio"], "mod must be one of 'quantit', 'visual' or 'audio'."

if config.args["phase"]:
    if input(f'eval={eval}, continue? y/n ').lower() == 'n':
        raise Error("Program stopped")

# Quantitative evaluation
if mod == "quanti":

    with open("runs/Runs_reconstruc_comparison.csv", "r") as f:
         reader = csv.reader(f, delimiter=',')
         for row in reader:
            if row[0] == model_name:
                raise ValueError("model is already in Runs_reconstruc_comparison.csv")
    origin = []
    reconstruct = []
    for index in range(test_dataset.__len__()):
        _, wavs, spectrograms, _, chunks, y_chunks, _, _ = test_dataset.magic(index)
        origin_wav, reconstruct_wav, origin_stft, reconstruct_stft, origin_y_mel, reconstruct_mel = \
                                                            get_matrix_reconstruction_steps(model, wavs, spectrograms, chunks, y_chunks, step, wl,
                                                                                            phase=config.args["phase"], eval=eval)
        print(f"Batch {index}")

        origin_y_mel = [(s - np.mean(s)) / np.std(s) for s in origin_y_mel]
        origin.append(origin_y_mel)
        reconstruct_mel = [(s - np.mean(s)) / np.std(s) for s in reconstruct_mel]
        reconstruct.append(reconstruct_mel)

    origin = np.concatenate(origin)
    reconstruct = np.concatenate(reconstruct)

    comp = substact_matrix(origin, reconstruct, kind='mel')
    mean = np.mean(comp)
    var = np.var(comp)
    std = np.std(comp)
    cor = tfp.stats.correlation(tf.reshape(origin, -1), tf.reshape(reconstruct, -1), sample_axis=0, event_axis=None).numpy()

    print(f"{model_name} : m={mean}, var={var}, ec={std}, cor={cor}")
    with open("runs/Runs_reconstruc_comparison.csv", "a", newline="") as f:
            writer = csv.writer(f)
            writer.writerow([model_name, mean, var, std, cor])
            writer.writerow([config.args['norm'], config.args['random_channel'], config.args['scale']])


# Visual evaluation
if mod == "visual":
    for i, (chunk, _, _) in enumerate(test_dataset):
        fig, axs = plot_reconstruction(model, chunk, using_phase=config.args["phase"])
        plt.savefig(f"plot/{model_name}_Random{config.args['random_channel']}_{i}")        
        plt.show()
        # if i == 2:
        #     break


# Audio evaluation
if mod == "audio":
    if os.path.exists(f"wav/{model_name}"):
        print(f"wav/{model_name} already exits, you might consider delete it (write_audio_GAIA_outputs function add files (does not overwrite))")
        if input('Continue? y/n ').lower() == 'y':
            write_audio_GAIA_outputs(test_dataset, model_name, model, step, wl, nb_batch=3)
            move_and_rename(model_name, "GAIA_outputs")
    else:
        write_audio_GAIA_outputs(test_dataset, model_name, model, step, wl, nb_batch=3)
        move_and_rename(model_name, "GAIA_outputs")
