import tensorflow as tf


def mish(x):
    return x * tf.nn.tanh(tf.nn.softplus(x))


class Print(tf.keras.layers.Layer):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def call(self, x, **kwargs):
        print(x.shape)
        return x