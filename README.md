# Rook_vocalisations_synthesis

You will find here the implementations made during my master 1 internship (long story short, we used a [Generative Adversarial Interpolative Autoencoding](https://github.com/timsainb/GAIA) based on Sainburg and colleagues work to generate rooks vocalisations). 

**Data** contains an example of data used (one zipped audio file and its annotations).

**Runs** contains examples of trained models (each file contains weights, losses and the run configuration).

**Scripts:**
-	_audio_generator.py_ will generate batch of Mel-spectrograms from data files
-	_config.py_ is the run’s configuration
-	_custom_layers.py_ contains the implementation of Mish activation
-	_evaluate.py_ allows to evaluate (quantitatively, visually, auditory) the runs
-	_function.py_ contains some functions needed for batch generation
-	_mel_to_audio.py_ contains functions needed to make and assess the conversion Mel-spectrogram to audio file using several methods
-	_plot_audio.py_ allows to plot together raw waveform, spectrogram and Mel-spectrogram for one vocalisation
-	_plot_interpolation.py_ allows to plot and compare interpolation from 2 runs
-	_set_config.py_ allows to perform several runs in a raw with config changes
-	_train.py_ will run the model with the config.py configuration

Note: this repo is not at its final state, I will improve it as soon as possible …
