from numpy.lib.function_base import iterable
import tensorflow as tf
from tensorflow_probability.python.distributions import Chi2
from custom_layers import mish
from custom_layers import Print
import config
import matplotlib.pyplot as plt
from function import model_size

def unet_convblock_down(
    _input,
    channels=16,
    kernel=(3, 3),
    activation=tf.nn.leaky_relu,
    pool_size=(2, 2),
    kernel_initializer="he_normal",
):
    """ An upsampling convolutional block for a UNET
    """
    conv = tf.keras.layers.Conv2D(
        channels,
        kernel,
        activation=activation,
        padding="same",
        kernel_initializer=kernel_initializer,
    )(_input)
    conv = tf.keras.layers.Conv2D(
        channels,
        kernel,
        activation=activation,
        padding="same",
        kernel_initializer=kernel_initializer,
    )(conv)
    pool = tf.keras.layers.MaxPooling2D(pool_size=pool_size)(conv)
    return conv, pool


def unet_convblock_up(
    last_conv,
    cross_conv,
    channels=16,
    kernel=(3, 3),
    activation=tf.nn.leaky_relu,
    pool_size=(2, 2),
    kernel_initializer="he_normal",
):
    """ A downsampling convolutional block for a UNET
    """
    up_conv = tf.keras.layers.UpSampling2D(size=pool_size)(last_conv)
    merge = tf.keras.layers.Concatenate(axis=3)([up_conv, cross_conv])
    conv = tf.keras.layers.Conv2D(
        channels,
        kernel,
        activation=activation,
        padding="same",
        kernel_initializer=kernel_initializer,
    )(merge)
    conv = tf.keras.layers.Conv2D(
        channels,
        kernel,
        activation=activation,
        padding="same",
        kernel_initializer=kernel_initializer,
    )(conv)
    return conv


def unet_function(DIMS, BATCH_SIZE, activation=tf.nn.leaky_relu, final_activation="tanh"):
    """ the architecture for a UNET specific to MNIST
    """
    inputs = tf.keras.layers.Input(batch_size=BATCH_SIZE, shape=DIMS)
    bn = tf.keras.layers.BatchNormalization(axis=-1)(inputs)
    up_1, pool_1 = unet_convblock_down(bn, channels=16, activation=activation)
    up_2, pool_2 = unet_convblock_down(pool_1, channels=32, activation=activation)
    up_3, pool_3 = unet_convblock_down(pool_2, channels=64, activation=activation)
    up_4, pool_4 = unet_convblock_down(pool_3, channels=128, activation=activation)
    conv_middle = tf.keras.layers.Conv2D(
        128, (3, 3), activation=activation, kernel_initializer="he_normal", padding="same"
    )(pool_4)
    conv_middle = tf.keras.layers.Conv2D(
        128, (3, 3), activation=activation, kernel_initializer="he_normal", padding="same"
    )(conv_middle)
    down_4 = unet_convblock_up(conv_middle, up_4, channels=128, activation=activation)
    down_3 = unet_convblock_up(down_4, up_3, channels=64, activation=activation)
    down_2 = unet_convblock_up(down_3, up_2, channels=32, activation=activation)
    down_1 = unet_convblock_up(down_2, up_1, channels=16, activation=activation)
    outputs = tf.keras.layers.Conv2D(1, (1, 1), activation=final_activation)(down_1)
    return inputs, outputs


def shared_encoder(N_Z, activation=tf.nn.leaky_relu, batchnorm=False):
    if batchnorm:
        layers = [tf.keras.layers.BatchNormalization(axis=-1),
        tf.keras.layers.Conv2D(filters=32, kernel_size=3, strides=(2, 2), activation=activation),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.Conv2D(filters=64, kernel_size=3, strides=(2, 2), activation=activation),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.Conv2D(filters=128, kernel_size=3, strides=(2, 2), activation=activation),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.Conv2D(filters=256, kernel_size=3, strides=(2, 2), activation=activation),
        tf.keras.layers.BatchNormalization(),
        tf.keras.layers.Conv2D(filters=256, kernel_size=3, strides=(1, 1), activation=activation),
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(units=N_Z)]
    else:
        layers = [tf.keras.layers.Conv2D(filters=32, kernel_size=3, strides=(2, 2), activation=activation),
        tf.keras.layers.Conv2D(filters=64, kernel_size=3, strides=(2, 2), activation=activation), 
        tf.keras.layers.Conv2D(filters=128, kernel_size=3, strides=(2, 2), activation=activation), 
        tf.keras.layers.Conv2D(filters=256, kernel_size=3, strides=(2, 2), activation=activation), 
        tf.keras.layers.Conv2D(filters=256, kernel_size=3, strides=(1, 1), activation=activation), 
        tf.keras.layers.Flatten(),
        tf.keras.layers.Dense(units=N_Z)]
    return layers


def shared_decoder(decDIMS, activation=tf.nn.leaky_relu, batchnorm=False):
    SIZE = 1
    for d in decDIMS:
        SIZE *= d
    if batchnorm:
        layers = [ tf.keras.layers.Dense(units=SIZE, activation=activation),
            tf.keras.layers.Reshape(target_shape=decDIMS),
            tf.keras.layers.Conv2DTranspose(filters=256, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv2DTranspose(filters=256, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv2DTranspose(filters=128, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv2DTranspose(filters=64, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv2DTranspose(filters=32, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
        ]
    else:
        layers = [
            tf.keras.layers.Dense(units=SIZE, activation=activation),
            tf.keras.layers.Reshape(target_shape=decDIMS),
            tf.keras.layers.Conv2DTranspose(filters=256, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
            tf.keras.layers.Conv2DTranspose(filters=256, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
            tf.keras.layers.Conv2DTranspose(filters=128, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
            tf.keras.layers.Conv2DTranspose(filters=64, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
            tf.keras.layers.Conv2DTranspose(filters=32, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
        ]
    return layers


def encoder(encDIMS, N_Z, BATCH_SIZE, activation=tf.nn.leaky_relu, batchnorm=False):
    if batchnorm:
        return [
            tf.keras.layers.Input(shape=encDIMS, batch_size=BATCH_SIZE),
            tf.keras.layers.BatchNormalization(axis=-1),
            tf.keras.layers.Conv2D(filters=32, kernel_size=3, strides=(2, 2), activation=activation),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv2D(filters=64, kernel_size=3, strides=(2, 2), activation=activation),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv2D(filters=128, kernel_size=3, strides=(2, 2), activation=activation),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv2D(filters=256, kernel_size=3, strides=(2, 2), activation=activation),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv2D(filters=256, kernel_size=3, strides=(1, 1), activation=activation),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(units=N_Z),
        ]
    else:
        return [
            tf.keras.layers.Input(shape=encDIMS, batch_size=BATCH_SIZE),
            tf.keras.layers.Conv2D(filters=32, kernel_size=3, strides=(2, 2), activation=activation),
            tf.keras.layers.Conv2D(filters=64, kernel_size=3, strides=(2, 2), activation=activation),
            tf.keras.layers.Conv2D(filters=128, kernel_size=3, strides=(2, 2), activation=activation),
            tf.keras.layers.Conv2D(filters=256, kernel_size=3, strides=(2, 2), activation=activation),
            tf.keras.layers.Conv2D(filters=256, kernel_size=3, strides=(1, 1), activation=activation),
            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(units=N_Z),
        ]


def decoder(decDIMS, activation=tf.nn.leaky_relu, final_activation="relu", batchnorm = False):
    SIZE = 1
    for d in decDIMS:
        SIZE *= d
    if batchnorm:
        return [
            tf.keras.layers.Dense(units=SIZE, activation=activation),
            tf.keras.layers.Reshape(target_shape=decDIMS),
            tf.keras.layers.Conv2DTranspose(filters=256, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv2DTranspose(filters=256, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv2DTranspose(filters=128, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv2DTranspose(filters=64, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Conv2DTranspose(filters=32, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
            tf.keras.layers.Conv2DTranspose(filters=1, kernel_size=3, strides=(1, 1), padding="SAME", activation=final_activation),
        ]
    else:
        return [
            tf.keras.layers.Dense(units=SIZE, activation=activation),
            tf.keras.layers.Reshape(target_shape=decDIMS),
            tf.keras.layers.Conv2DTranspose(filters=256, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
            tf.keras.layers.Conv2DTranspose(filters=256, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
            tf.keras.layers.Conv2DTranspose(filters=128, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
            tf.keras.layers.Conv2DTranspose(filters=64, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
            tf.keras.layers.Conv2DTranspose(filters=32, kernel_size=3, strides=(2, 2), padding="SAME", activation=activation),
            tf.keras.layers.Conv2DTranspose(filters=1, kernel_size=3, strides=(1, 1), padding="SAME", activation=final_activation),
        ]


class GAIA(tf.keras.Model):
    """a basic gaia class for tensorflow
    Extends:
        tf.keras.Model
    """

    def __init__(self, gen_optimizer, disc_optimizer, unet_list, interpolate_function, using_phase = False, interpolate_kwargs = None, d_prop_xg=1.0, g_prop_interp=1.0, **kwargs):
        super().__init__()
        self.__dict__.update(kwargs)
        self.using_phase = using_phase
        assert using_phase in [False, "separate", "shared"], "using_phase must be one of False, 'shared' or 'separate'."
        if not using_phase and isinstance(self.final_activation, list):
            self.final_activation = self.final_activation[0]
        elif using_phase and not isinstance(self.final_activation, list):
            raise ValueError(f"using_phase is {using_phase} but final_activation is not a list of length 2")

        if self.using_phase == "shared":
            self.gen_optimizer = gen_optimizer
            self.disc_optimizer = disc_optimizer
            enc_kwargs = {k: self.__dict__[k] for k in self.__dict__.keys() if k in shared_encoder.__code__.co_varnames}
            dec_kwargs = {k: self.__dict__[k] for k in self.__dict__.keys() if k in shared_decoder.__code__.co_varnames}
            self.enc_layers = shared_encoder(**enc_kwargs)
            self.dec_layers = shared_decoder(**dec_kwargs)  ############## () > (self.decDIMS)
            self.enc_mag = tf.keras.Sequential([self.enc_input_mag, *self.enc_layers])
            self.enc_phase = tf.keras.Sequential([self.enc_input_phase, *self.enc_layers])
            self.dec_mag = tf.keras.Sequential([*self.dec_layers, self.dec_out_mag])
            self.dec_phase = tf.keras.Sequential([*self.dec_layers, self.dec_out_phase])

            inputs, outputs = unet_list
            self.disc = tf.keras.Model(inputs=[inputs], outputs=[outputs])

        elif self.using_phase == "separate":
            enc_kwargs = {k: self.__dict__[k] for k in self.__dict__.keys() if k in self.enc.__code__.co_varnames}
            dec_kwargs = {k: self.__dict__[k] for k in self.__dict__.keys() if k in self.dec.__code__.co_varnames}
            dec_kwargs.pop('final_activation')
            # mag
            self.mag_gen_optimizer = gen_optimizer[0]
            self.mag_disc_optimizer = disc_optimizer[0]
            self.enc_mag = tf.keras.Sequential(self.enc(**enc_kwargs))
            self.dec_mag = tf.keras.Sequential(self.dec(**dec_kwargs, final_activation=self.final_activation[0]))
            mag_inputs, mag_outputs = unet_list
            self.mag_disc = tf.keras.Model(inputs=[mag_inputs], outputs=[mag_outputs])
            # phase
            self.phase_gen_optimizer = gen_optimizer[1]
            self.phase_disc_optimizer = disc_optimizer[1]
            self.enc_phase = tf.keras.Sequential(self.enc(**enc_kwargs))
            self.dec_phase = tf.keras.Sequential(self.dec(**dec_kwargs, final_activation=self.final_activation[1]))
            phase_inputs, phase_outputs = unet_list
            self.phase_disc = tf.keras.Model(inputs=[phase_inputs], outputs=[phase_outputs])
        else:
            enc_kwargs = {k: self.__dict__[k] for k in self.__dict__.keys() if k in self.enc.__code__.co_varnames}
            dec_kwargs = {k: self.__dict__[k] for k in self.__dict__.keys() if k in self.dec.__code__.co_varnames}
            self.gen_optimizer = gen_optimizer
            self.disc_optimizer = disc_optimizer
            self.enc = tf.keras.Sequential(self.enc(**enc_kwargs))
            self.dec = tf.keras.Sequential(self.dec(**dec_kwargs))
            inputs, outputs = unet_list
            self.disc = tf.keras.Model(inputs=[inputs], outputs=[outputs])

        self.d_prop_xg = d_prop_xg
        self.g_prop_interp = g_prop_interp
        self.interpolate_function = interpolate_function
        self.interpolate_kwargs = interpolate_kwargs or dict()

        if self.using_phase == "separate":
            self.network_method = self.network_pass_separate
            self.compute_gradients_method = self.compute_gradients_separate
            self.apply_gradients_method = self.apply_gradients_separate
        elif self.using_phase == "shared":
            self.network_method = self.network_pass_shared
            self.compute_gradients_method = self.compute_gradients_shared
            self.apply_gradients_method = self.apply_gradients_shared
        else:
            self.network_method = self.network_pass
            self.compute_gradients_method = self.compute_gradients
            self.apply_gradients_method = self.apply_gradients

        self.built_ = False
        self.axis = None

    def build_on_input(self, shape):
        if self.using_phase == "separate":
            self.axis = list(range(len(shape)))
            self.axis.pop(-2)
        self.built_ = True

    def regularization(self, x1, x2, sample_weight=None):
        if not self.built_:
            self.build_on_input(x1.shape)
        masked = tf.square(x1 - x2)
        return tf.reduce_mean(masked, axis=self.axis)

    def network_pass(self, x):
        z = self.enc(x)
        xg = self.dec(z)
        zi = self.interpolate_function(z, **self.interpolate_kwargs)
        xi = self.dec(zi)
        d_xi = self.disc(xi)
        d_x = self.disc(x)
        d_xg = self.disc(xg)
        return z, xg, zi, xi, d_xi, d_x, d_xg

    def network_pass_shared(self, x):
        x = tf.unstack(x, axis=-2)
        # mag
        z_mag = self.enc_mag(x[0])
        xg_mag = self.dec_mag(z_mag)
        zi_mag = self.interpolate_function(z_mag, **self.interpolate_kwargs)
        xi_mag = self.dec_mag(zi_mag)
        d_xi_mag = self.disc(xi_mag)
        d_x_mag = self.disc(x[0])
        d_xg_mag = self.disc(xg_mag)
        # phase
        z_phase = self.enc_phase(x[1])
        xg_phase = self.dec_phase(z_phase)
        zi_phase = self.interpolate_function(z_phase, **self.interpolate_kwargs)
        xi_phase = self.dec_phase(zi_phase)
        d_xi_phase = self.disc(xi_phase)
        d_x_phase = self.disc(x[1])
        d_xg_phase = self.disc(xg_phase)
        #stack
        z = tf.stack([z_mag, z_phase], axis=-2)
        xg = tf.stack([xg_mag, xg_phase], axis=-2)
        zi = tf.stack([zi_mag, zi_phase], axis=-2)
        xi = tf.stack([xi_mag, xi_phase], axis=-2)
        d_xi = tf.stack([d_xi_mag, d_xi_phase], axis=-2)
        d_x = tf.stack([d_x_mag, d_x_phase], axis=-2)
        d_xg = tf.stack([d_xg_mag, d_xg_phase], axis=-2)
        return z, xg, zi, xi, d_xi, d_x, d_xg

    def network_pass_separate(self, x):
        x = tf.unstack(x, axis=-2)
        # mag
        z_mag = self.enc_mag(x[0])
        xg_mag = self.dec_mag(z_mag)
        zi_mag = self.interpolate_function(z_mag, **self.interpolate_kwargs)
        xi_mag = self.dec_mag(zi_mag)
        d_xi_mag = self.mag_disc(xi_mag)
        d_x_mag = self.mag_disc(x[0])
        d_xg_mag = self.mag_disc(xg_mag)
        # phase
        z_phase = self.enc_phase(x[1])
        xg_phase = self.dec_phase(z_phase)
        zi_phase = self.interpolate_function(z_phase, **self.interpolate_kwargs)
        xi_phase = self.dec_phase(zi_phase)
        d_xi_phase = self.phase_disc(xi_phase)
        d_x_phase = self.phase_disc(x[1])
        d_xg_phase = self.phase_disc(xg_phase)
        #stack
        z = tf.stack([z_mag, z_phase], axis=-2)
        xg = tf.stack([xg_mag, xg_phase], axis=-2)
        zi = tf.stack([zi_mag, zi_phase], axis=-2)
        xi = tf.stack([xi_mag, xi_phase], axis=-2)
        d_xi = tf.stack([d_xi_mag, d_xi_phase], axis=-2)
        d_x = tf.stack([d_x_mag, d_x_phase], axis=-2)
        d_xg = tf.stack([d_xg_mag, d_xg_phase], axis=-2)
        return z, xg, zi, xi, d_xi, d_x, d_xg

    def call(self, inputs, **kwargs):
        return self.network_method(inputs)

    def compute_loss(self, x, y, sample_weight=None):
        z, xg, zi, xi, d_xi, d_x, d_xg = self.network_method(x)

        X_G_X_loss = tf.clip_by_value(self.regularization(y, xg, sample_weight=sample_weight), 0, 1)
        X_D_G_X_loss = tf.clip_by_value(self.regularization(y, d_xg, sample_weight=sample_weight), 0, 1)
        X_D_G_Zi_loss = tf.clip_by_value(self.regularization(xi, d_xi, sample_weight=sample_weight), 0, 1)
        X_G_loss = (X_D_G_Zi_loss + X_D_G_X_loss) / 2.0  #
        X_D_X_loss = tf.clip_by_value(self.regularization(y, d_x, sample_weight=sample_weight), 0, 1)

        D_loss = (X_D_X_loss + X_D_G_X_loss - tf.clip_by_value(X_D_G_Zi_loss, 0, X_D_X_loss) * self.d_prop_xg)
        G_loss = (X_G_X_loss + tf.clip_by_value(X_D_G_Zi_loss, 0, X_D_X_loss) * self.g_prop_interp)

        return (X_D_G_X_loss, X_D_G_Zi_loss, X_G_loss, X_D_X_loss, X_G_X_loss, G_loss, D_loss)

    def compute_gradients(self, x, y):
        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
            _, _, _, _, _, G_loss, D_loss = self.compute_loss(x, y)
            gen_loss = G_loss
            disc_loss = D_loss
        gen_gradients = gen_tape.gradient(gen_loss, self.enc.trainable_variables + self.dec.trainable_variables)
        disc_gradients = disc_tape.gradient(disc_loss, self.disc.trainable_variables)
        return gen_gradients, disc_gradients
    
    def compute_gradients_shared(self, x, y):
        with tf.GradientTape() as gen_tape, tf.GradientTape() as disc_tape:
            _, _, _, _, _, G_loss, D_loss = self.compute_loss(x, y)
            gen_loss = G_loss
            disc_loss = D_loss
            gen_gradients = gen_tape.gradient(gen_loss, 
                                            self.enc_mag.trainable_variables + self.dec_mag.trainable_variables + \
                                            self.enc_phase.trainable_variables + self.dec_phase.trainable_variables)
            disc_gradients = disc_tape.gradient(disc_loss, self.disc.trainable_variables)
        return gen_gradients, disc_gradients

    def compute_gradients_separate(self, x, y):
        with tf.GradientTape() as gen_mag_tape, tf.GradientTape() as gen_phase_tape, tf.GradientTape() as disc_mag_tape, tf.GradientTape() as disc_phase_tape:
            _, _, _, _, _, G_loss, D_loss = self.compute_loss(x, y)
            gen_loss = tf.unstack(G_loss, axis=0)
            disc_loss = tf.unstack(D_loss, axis=0)
            # mag
            mag_gen_gradients = gen_mag_tape.gradient(gen_loss[0], self.enc_mag.trainable_variables + self.dec_mag.trainable_variables)
            mag_disc_gradients = disc_mag_tape.gradient(disc_loss[0], self.mag_disc.trainable_variables)
            #phase
            phase_gen_gradients = gen_phase_tape.gradient(gen_loss[1], self.enc_phase.trainable_variables + self.dec_phase.trainable_variables)
            phase_disc_gradients = disc_phase_tape.gradient(disc_loss[1], self.phase_disc.trainable_variables)
        return mag_gen_gradients, phase_gen_gradients, mag_disc_gradients, phase_disc_gradients


    def apply_gradients(self, gen_gradients, disc_gradients):
        self.gen_optimizer.apply_gradients(
            zip(gen_gradients,self.enc.trainable_variables + self.dec.trainable_variables))
        self.disc_optimizer.apply_gradients(
            zip(disc_gradients, self.disc.trainable_variables))

    def apply_gradients_shared(self, gen_gradients, disc_gradients):
        self.gen_optimizer.apply_gradients(
            zip(gen_gradients,
                self.enc_mag.trainable_variables + self.dec_mag.trainable_variables + \
                self.enc_phase.trainable_variables + self.dec_phase.trainable_variables))
        self.disc_optimizer.apply_gradients(
            zip(disc_gradients, self.disc.trainable_variables))

    def apply_gradients_separate(self, mag_gen_gradients, phase_gen_gradients, mag_disc_gradients, phase_disc_gradients):
        # mag
        self.mag_gen_optimizer.apply_gradients(
            zip(mag_gen_gradients, self.enc_mag.trainable_variables + self.dec_mag.trainable_variables))
        self.mag_disc_optimizer.apply_gradients(
            zip(mag_disc_gradients, self.mag_disc.trainable_variables))
        # phase
        self.phase_gen_optimizer.apply_gradients(
            zip(phase_gen_gradients, self.enc_phase.trainable_variables + self.dec_phase.trainable_variables))
        self.phase_disc_optimizer.apply_gradients(
            zip(phase_disc_gradients, self.phase_disc.trainable_variables))


    def train_net(self, x, y):
        gradients = self.compute_gradients_method(x, y)
        self.apply_gradients_method(*gradients)


def interpolate_z_GAIA(z, chsq):
    ip = chsq.sample((z.shape[0], z.shape[0]))
    ip = ip / tf.reduce_sum(ip, axis=0)
    zi = tf.transpose(tf.tensordot(tf.transpose(z), ip, axes=1))
    return zi

def interpolate_z_VAE(z, chsq):
    z_mean = tf.math.reduce_mean(z)
    z_std = tf.math.reduce_std(z)
    zi = tf.random.normal(z.shape, mean=z_mean, stddev=z_std, dtype=z.dtype)
    return zi


def sigmoid(x, shift=0.0, mult=20):
    return tf.constant(1.0) / (
        tf.constant(1.0) + tf.exp(-tf.constant(1.0) * ((x + tf.constant(shift)) * mult))
    )


def plot_reconstruction(model, example_data, nex=5, zm=3, using_phase=False):
    z, xg, zi, xi, d_xi, d_x, d_xg = model(example_data)
    if using_phase:
        (example_data, xg, xi, d_xi, d_x, d_xg) = (c[..., 0, :] for c in [example_data, xg, xi, d_xi, d_x, d_xg])
    fig, axs = plt.subplots(ncols=6, nrows=nex, figsize=(zm * 6, zm * nex))
    for axi, (dat, lab) in enumerate(
        zip(
            [example_data, d_x, xg, d_xg, xi, d_xi],
            ["data", "disc data", "gen", "disc gen", "interp", "disc interp"],
        )
    ):
        for ex in range(nex):
            axs[ex, axi].matshow(
                dat.numpy()[ex].squeeze(), vmin=0, vmax=1
            )
            axs[ex, axi].invert_yaxis()
            axs[ex, axi].axis("off")
        axs[0, axi].set_title(lab)
    return fig, axs        
    # plt.show()
