import tensorflow as tf
import os
import re
import soundfile as sf
from tensorflow.python.keras.utils.data_utils import Sequence
import librosa
import tensorflow.keras.backend as K
import numpy as np
import random
from copy import deepcopy
from function import *
import pandas as pd
import scipy.signal as sig
import config as config

"""
Author : Killian Martin
"""


class BaseAudioGenerator(Sequence):
    def __init__(self,
                 data,
                 batch_size,
                 duration,
                 wl, ovlp, window,
                 n_mels,
                 n_times,
                 col=None,
                 remove_offset=True,
                 fmin=0, fmax=np.Inf,
                 power=1.,
                 augment_function=None,
                 training=False,
                 pad_type="zeros",
                 dtype="float32",
                 keepdims=True,
                 labels_format="tsv",
                 audio_format="wav",
                 n_channels=None,
                 exclude=None,
                 multilabel_class=None,
                 negative_prop=0.,
                 phase=False,
                 scale=False,
                 norm=True,
                 random_channel=False,
                 test=False,
                 **kwargs):
        """
        Generates melspectrograms and labels to be fed into a neural network.
        Takes audio files as well as label files containing the start and end times of the vocalisations of interest.

        Generation of a batch involves randomly taking chunks out of all the audio files given as data, remembering the
        time stamps, then labelling them according to intersection with the given labels.

        Unless otherwise noted, all parameters used in creation of the mel-spectrogram default to their default value
        in librosa.

        :param data: list or str. Paths to each of the audio files. If list, pass as is. If passed as str, check if
        corresponds to a directory; if it is, take all the files within that directory, else pass as list of length 1.
        :param labels: list or str.  Paths to each of the label files. If list, pass as is. If passed as str, check if
        corresponds to a directory; if it is, take all the files within that directory, else pass as list of length 1.
        :param batch_size: int. Batch size for training. Passed as is.
        :param duration: float or list. Determines the duration of the audio chunks. If given as a single float, every
        chunk in every batch will be of the same duration. If given as a list, a drawing will be done every batch to
        determine the duration of the chunks: for a list of length 2, draw from a uniform distribution bounded by the
        two values; for a list of length > 2, draw one of the values.
        :param wl: int. Length of a single frame of the melspectrogram, in samples.
        :param ovlp: float. The overlap between successive frames. If 0<ovlp<1, used as is. If 1<=ovlp, divided
        by 100 (considered as percent of wl)
        :param window: str of function object. The window function to use for the melspectrogram.
        :param n_mels: int. Number of mel-frequency bands in the melspectrogram. Mostly corresponds to dimensionality
        reduction (if set lower than or equal to wl * sr // 2 ).
        :param col: str. Column of data carrying the class labels.
        :param fmin: int. Minimum frequency (in Hz) for the melspectrogram.
        :param fmax: int. Maximum frequency (in Hz) for the melspectrogram. If not provided, will be determined from the
        sample rates sr (such that fmax = int(min(sr)/2) ).
        :param power: int or float. Power to apply to the spectrogram prior to the conversion to melspectrogram.
        :param remove_offset: bool. Whether (True) or not (False) to remove the DC offset from samples (in practice,
        substract the mean of the sample from its values).
        :param augment_function: The function to use to perform data augmentation on X and y. Should be a function with
        all its parameters set beforehand, except for X and y, and returns a tuple of corresponding lists of augmented X
        and y. Defaults to None, in which case no augmentation is performed
        :param training: Whether or not the generator is intended for training (True) or validation/testing/prediction (False).
        :param kwargs: Further optional arguments
        """
        # determine channel axis for expansion at the end
        self.pad_type = pad_type
        self.channel_axis = 0 if K.image_data_format() == "channels_first" else -1
        self.time_axis = 2 + self.channel_axis

        # generator parameters
        self.batch_size = batch_size
        self.duration = duration
        self.scale = scale
        self.norm = norm
        self.training = training
        self.augment_func = augment_function if augment_function is not None and self.training else None
        self.keepdims = keepdims
        self.exclude = exclude or []
        self.multilabel_class = multilabel_class or []

        # data parameters
        self.labels_format = labels_format
        self.audio_format = audio_format
        self.col = col.lower() if col else None

        # audio
        self.data = data
        if isinstance(self.data, str):
            self.data = [os.path.join(self.data, _) for _ in os.listdir(self.data)]

        self.data_names = [_ for _ in self.data if self.audio_format in _ and not _.startswith(".")]
        self.data_names = sorted(self.data_names)
        self.data_objects = [sf.SoundFile(name) for name in self.data_names]
        self.data_objects = dict(zip(self.data_names, self.data_objects))
        # labels
        self.labels = [_ for _ in self.data if self.labels_format in _ and not _.startswith(".")]
        self.labels = sorted(self.labels) if len(self.labels) > 0 else None

        # get relevant informations from the audio files
        self.data_info = [sf.info(file=_) for _ in self.data_names]
        self.lengths = dict(zip(self.data_names, [self.data_info[_].duration for _ in range(len(self.data_names))]))
        self.total = sum(self.lengths.values())
        # self.sr = dict(zip(self.data_names, [self.data_info[_].samplerate for _ in range(len(self.data_names))]))
        self.sr = [self.data_info[_].samplerate for _ in range(len(self.data_names))]
        if len(set(self.sr)) > 1:
            raise ValueError("Files of multiple sample rates are not currently supported")
        self.sr = self.sr[0]
        self.channels = dict(zip(self.data_names, [self.data_info[_].channels for _ in range(len(self.data_names))]))
        self.max_channels = n_channels or max(self.channels.values())

        # make sure to match each audio file to a corresponding label file
        # here: find the date of each file
        dates = [re.search(pattern="[0-9]{8}", string=_).group() for _ in self.data_names]
        self.data_names = [_ for _ in self.data_names if any([re.search(pattern=i, string=_) for i in dates])]
        if len(self.data_names) != len(self.labels):
            raise ValueError(f"{len(self.data_names)} audio files and {len(self.labels)} label files have been found, "
                             f"but the same number of each should be present. ")

        # create label data
        self.labels = [pd.read_table(_, engine="python") for _ in self.labels]
        self.labels = [df.assign(name=self.data_names[i]) for i, df in enumerate(self.labels)]
        # concatenate into one data frame
        for l in self.labels:
            l.columns = map(str.lower, l.columns)
        self.labels = pd.concat(self.labels, ignore_index=True, axis=0)

        # parameters for melspectrogram
        self.test = test
        self.phase = phase
        self.random_channel = random_channel
        self.remove_offset = remove_offset
        self.wl = int(wl * self.sr)
        self.ovlp = ovlp if 0. < ovlp < 1. else ovlp / 100
        self.step = int(self.wl * (1-self.ovlp))
        self.window = window
        self.n_mels = n_mels
        self.n_times = n_times
        self.power = power
        self.fmin = fmin
        self.fmax = min(fmax, self.sr // 2)
        self.dtype = dtype

        if self.fmin > 0.:
            self.wav_filter = sig.butter(N = 6, Wn = self.fmin / self.sr, btype = "high")
        self.filters = librosa.filters.mel(sr=self.sr,
                                           n_fft=self.wl,
                                           n_mels=self.n_mels,
                                           fmin=self.fmin,
                                           fmax=self.fmax,
                                           htk=True)
        self.filters = tf.transpose(self.filters)
        self.fps = self.sr // self.step
        self.frame_dur = 1 / self.fps
        self.frames = self.duration * self.fps

        # Set proportions of positive and negative samples per batch (ignored if self.col is None)
        if self.training:
            self.negative_prop = negative_prop
        else:
            self.negative_prop = 0.
        self.positive_prop = 1. - self.negative_prop

    def __len__(self):
        """
        Returns the number of batches in an epoch of training.

        :return: int number of batches in an epoch
        """
        # NB: twice the number of samples so that roughly half of the samples should be negative, no matter what
        length = int(self.labels["k"].sum() // self.batch_size / self.positive_prop)
        return length

    def on_epoch_end(self):
        self.labels = self.labels.assign(used=0)

    def extract_starts(self, duration, index):
        """
        Basic extraction routine, with random starts and no a priori (e.g. not even trying to balance the classes)
        """
        names = random.choices(self.data_names, k=self.batch_size)
        lengths = map(self.lengths.get, names)
        starts = map(lambda n: random.uniform(0, n - duration), lengths)
        return names, starts, None, None

    def target_read(self, name, start, duration):
        wav = self.data_objects.get(name)
        wav.seek(int(self.sr * start))
        chunk = wav.read(frames=int(self.sr * duration), always_2d=True, dtype=self.dtype)
        return chunk

    def chunk_batch(self, duration, index):
        """
        Apply extract_chunk to a batch of data, first choosing the names of the files to extract from (by repeatedly
        drawing of names from the list of all file names), then applying extract_chunk to each in time.

        :return: tuple of the names, offsets and data extracted
        """
        names, starts, ends, channels = self.extract_starts(duration=duration, index=index)
        #channels = map(self.channels.get, names)
        chunks = [self.target_read(name=n, start=s, duration=e-s) for n, s, e in zip(names, starts, ends)]

        return names, starts, ends, chunks, channels

    def label_gen(self, name, start, end):
        """
        Defines the label generation method. Should take all the specified arguments.
        Overwrite by subclassing.
        """
        raise NotImplementedError("label_gen method not implemented")

    def log_scaling(self, x):
        return self.n_times * tf.math.log(float(x)) / tf.math.log(tf.constant(self.n_times, dtype=self.dtype))

    def time_frequency_transform(self, chunk):
        chunk = tf.signal.stft(chunk,
                               frame_length=self.wl,
                               fft_length=self.wl,
                               frame_step=self.step,
                               window_fn=self.window,
                               pad_end=True)
        if self.phase:
            chunk_phase = tf.math.angle(chunk)
            chunk = tf.pow(tf.abs(chunk), self.power)
            chunk = tf.stack([chunk, chunk_phase], axis=-1)
        else:
            chunk = tf.pow(tf.abs(chunk), self.power)
        if self.scale:
            chunk = tf.squeeze(tf.image.resize(tf.expand_dims(chunk, axis=-1), size=(tf.round(self.log_scaling(chunk.shape[0])), chunk.shape[1])))

        return chunk

    def magic(self, index):
        """Generate one batch of data"""
        # Pick duration of all chunks for the batch
        if isinstance(self.duration, int) or isinstance(self.duration, float):
            duration = self.duration  # if given as unique value
        elif isinstance(self.duration, list) and len(self.duration) == 1:
            duration = self.duration[0]
        elif isinstance(self.duration, list) and len(self.duration) == 2:
            duration = random.uniform(self.duration[0], self.duration[1])  # if given as range of values
        else:
            duration = self.duration[random.randrange(len(self.duration))]  # if given as list of values

        # Generate the raw batch
        names, starts, ends, chunks, channels = self.chunk_batch(duration, index)

        if self.random_channel:
            if self.test:
                wavs = [tf.unstack(c, axis=-1) for c in chunks]
                names = flatten([[n]*len(w) for n, w in zip(names, wavs)])
                starts = flatten([[s]*len(w) for s, w in zip(starts, wavs)])
                ends = flatten([[e]*len(w) for e, w in zip(ends, wavs)])
                wavs = flatten(wavs)
            else: 
                wavs = [c[..., random.randrange(c.shape[self.channel_axis])] for c, channel in zip(chunks, channels)]
        else:
            wavs = [c[..., int(channel)] for c, channel in zip(chunks, channels)]

        if self.fmin > 0.:
            wavs = [sig.lfilter(*self.wav_filter, x=w) for w in wavs]

        # Create the melspectrograms
        if self.phase:
            spectrograms = [tf.transpose(self.time_frequency_transform(c), perm=(1, 0, 2)) for c in wavs]
        else:
            spectrograms = [tf.transpose(self.time_frequency_transform(c)) for c in wavs]

        # Create the labels
         # return the (non-augmented) inputs as ground truth (e.g. for an autoencoder, denoiser...)
        y_chunks = deepcopy(spectrograms)
        y_binary, masks, label_df = zip(*[self.label_gen(name=n, start=s, end=e) for n, s, e in zip(names, starts, ends)])
        y_chunks = [c if i == 1. else tf.zeros_like(c) for c, i in zip(y_chunks, y_binary)]
        if self.augment_func and self.training:
            spectrograms, _, masks = self.augment_func(spectrograms, [y_chunks, y_binary], masks)

        # Pad missing dimensions with 0s
        max_shape = [_.shape for _ in spectrograms]
        max_shape = [max(_) for _ in zip(*max_shape)]
        max_shape[self.time_axis] = self.n_times
        chunks = [tf.pad(c,
                         paddings=[[0, max_shape[j] - c.shape[j]] for j in range(len(c.shape))],
                         mode="constant",
                         constant_values=0) if c.shape != max_shape else c for c in spectrograms]
        y_chunks = [tf.pad(c,
                           paddings=[[0, max_shape[j] - c.shape[j]] for j in range(len(c.shape))],
                           mode="constant",
                           constant_values=0) if c.shape != max_shape else c for c in y_chunks]

        # Shuffle chunks and labels DURING TRAINING ONLY
        if self.training:
            shuffling_list = list(zip(chunks, y_binary, y_chunks, masks))
            random.shuffle(shuffling_list)
            chunks, y_binary, y_chunks, masks = zip(*shuffling_list)

        # stack along the batch axis #####
        chunks = tf.stack(chunks, axis=0)
        y_binary = tf.stack(y_binary, axis=0)
        y_chunks = tf.stack(y_chunks, axis=0)
        masks = None
        if masks is not None:
            masks = tf.stack(masks, axis=0)

        # Apply Mel Filterbank
        if self.phase:
            # (batch, mag/phase, freq, time, channels)
            chunks = tf.transpose(chunks, perm=[0, 3, 2, 1])
            chunks = tf.matmul(chunks, self.filters)
            chunks = tf.transpose(chunks, perm=[0, 3, 2, 1])
            y_chunks = tf.transpose(y_chunks, perm=[0, 3, 2, 1])
            y_chunks = tf.matmul(y_chunks, self.filters)
            y_chunks = tf.transpose(y_chunks, perm=[0, 3, 2, 1])
        else:
            # chunks, freq, time, channels
            chunks = tf.transpose(chunks, perm=[0, 2, 1])
            chunks = tf.matmul(chunks, self.filters)
            chunks = tf.transpose(chunks, perm=[0, 2, 1])
            y_chunks = tf.transpose(y_chunks, perm=[0, 2, 1])
            y_chunks = tf.matmul(y_chunks, self.filters)
            y_chunks = tf.transpose(y_chunks, perm=[0, 2, 1])

        chunks = tf.expand_dims(chunks, axis=self.channel_axis)
        y_chunks = tf.expand_dims(y_chunks, axis=self.channel_axis)

        if chunks.shape[0] < self.batch_size:
            paddings = [(0, 0)] * max(chunks.ndim, y_chunks.ndim, y_binary.ndim)
            paddings[0] = (0, self.batch_size - chunks.shape[0])
            chunks = tf.pad(chunks, paddings=paddings[:chunks.ndim], constant_values=0.)
            y_chunks = tf.pad(y_chunks, paddings=paddings[:y_chunks.ndim], constant_values=0.)
            y_binary = tf.pad(y_binary, paddings=paddings[:y_binary.ndim], constant_values=0.)
            if masks is not None:
                masks = tf.pad(masks, paddings=paddings[:tf.rank(masks)], constant_values=0.)

        chunks_un = deepcopy(chunks)

        if self.phase:
            chunks = tf.unstack(chunks, axis=-2)
            chunks[0] = tf.pow(chunks[0], 1./3.)
            if self.norm:
                chunks[0] = tf.math.divide_no_nan(chunks[0], tf.reduce_max(chunks[0], axis=list(range(1, len(chunks[0].shape))), keepdims=True))
            chunks = tf.stack(chunks, axis=-2)

            y_chunks = tf.unstack(y_chunks, axis=-2)
            y_chunks[0] = tf.pow(y_chunks[0], 1./3.)
            if self.norm:
                y_chunks[0] = tf.math.divide_no_nan(y_chunks[0],
                                                    tf.reduce_max(y_chunks[0],
                                                                  axis=list(range(1, len(y_chunks[0].shape))), keepdims=True))
            y_chunks = tf.stack(y_chunks, axis=-2)
        else:
            chunks = tf.pow(chunks, 1./3.)
            y_chunks = tf.pow(y_chunks, 1./3.)
            if self.norm:
                chunks = tf.math.divide_no_nan(chunks, tf.reduce_max(chunks, axis=list(range(1, len(chunks.shape))), keepdims=True))
                y_chunks = tf.math.divide_no_nan(y_chunks, tf.reduce_max(y_chunks, axis=list(range(1, len(chunks.shape))), keepdims=True))
        return pd.concat(label_df, 0), wavs, spectrograms, chunks_un, chunks, y_chunks, y_binary, masks

    def __getitem__(self, index):
        label_df, wavs, spectrograms, chunks_un, chunks, y_chunks, y_binary, masks = self.magic(index)
        return chunks, y_chunks, masks


class TrainingGenerator(BaseAudioGenerator):
    def __init__(self,
                 classes=None,
                 k=1.,
                 training=True,
                 *args, **kwargs):
        super().__init__(training=training, *args, **kwargs)

        # Class proportions
        if self.training:
            self.class_prop = self.labels.assign(duration=lambda row: row.end - row.start)
            if self.col:
                self.class_prop = self.class_prop.groupby(self.col) \
                    .agg(samples=pd.NamedAgg(column="duration", aggfunc="count"),
                         duration=pd.NamedAgg(column="duration", aggfunc="sum"))
                self.class_prop["prop"] = self.class_prop["duration"] / self.class_prop["duration"].sum()
                # Drop excluded and multilabel classes
                self.class_prop = self.class_prop.drop(index=self.exclude + self.multilabel_class)
            else:
                self.class_prop = self.class_prop.agg(duration=pd.NamedAgg(column="duration", aggfunc="sum"), func=None)
                self.class_prop["prop"] = self.class_prop["duration"] / self.total

            # Define the number of times (k) each row might be reused
            # self.class_prop = self.class_prop.assign(k=lambda row: (1/row.prop).astype(int))
            self.class_prop["k"] = self.class_prop["prop"].max() / self.class_prop["prop"]

        # Class labels
        if classes:
            self.classes = sorted(classes)
        else:
            self.classes = list(self.class_prop.index) if self.col else None
        if self.classes:
            self.classes = [c for c in self.classes if c not in self.exclude + self.multilabel_class]

        if self.multilabel_class:
            # replace a row containing a multiclass label by multiple rows corresponding
            # to each class individually, otherwise copying the values (notably Starts and Ends)
            self.labels = self.labels.assign(matches=self.labels["comment"].str.findall("|".join(self.classes)))
            self.labels = self.labels.explode('matches').fillna('')
            self.labels[self.col] = np.where(self.labels[self.col].isin(self.multilabel_class), self.labels['matches'],
                                             self.labels[self.col])

        # Negative samples
        self.negative_samples = []
        if self.negative_prop > 0.:
            # concatenate into one data frame
            for n in self.data_names:
                interval_merge = interval_union(self.labels.loc[self.labels.name == n], "start", "end", inplace=False)
                neg_samples = pd.DataFrame({
                    "name": [n] * (interval_merge.shape[0] + 1),
                    "start": [0] + interval_merge["end"].tolist(),
                    "end": interval_merge["start"].tolist() + [self.lengths.get(n)]
                })
                neg_samples = neg_samples[neg_samples["end"] - neg_samples["start"] > self.duration]
                self.negative_samples.append(neg_samples)
            self.negative_samples = pd.concat(self.negative_samples, ignore_index=True, axis=0)

        # removes unwanted vocs
        if not self.random_channel:
            self.labels.dropna(subset=['channel'])
        self.labels = to_keep(self.labels,
                              sources_to_del=self.exclude,
                              events_to_del=['sing'],
                              max_duration=self.n_times/self.fps)

        # Number classes
        self.n_classes = len(self.classes) if self.classes else 1  # 1 for binary classification
        if self.classes is not None:
            self.class_id = dict(zip(self.classes, range(self.n_classes)))
            self.class_id.update(dict(zip(self.exclude, [-1] * len(self.exclude))))
            self.labels["class_id"] = self.labels[self.col].map(self.class_id).astype("int")

        # Resample labels if necessary
        self.k = k
        if self.training:
            if self.k is None:
                # repeat rows according to the (rounded) inverse proportion of the respective classes
                if self.col:
                    self.labels["k"] = self.labels[self.col].map(self.class_prop['k'])
                else:
                    self.k = self.class_prop.iloc[0]
            elif self.k < 1:
                self.labels = self.labels.sample(frac=k)
        else:
            self.k = 1


        # Initialise the counts of used times for each row of labels
        if "k" not in self.labels.columns:
            self.labels['k'] = max(self.k or 1, 1)
        self.labels = self.labels.assign(used=0)

    def extract_starts(self, duration, index):
        """
        :param duration: float, duration in seconds of the chunks

        :returns: tuple of length 2 containing the lists of filenames and start points (in seconds)
        """
        # select rows that have been used less than self.k times (allowing for class-specific k)
        df = self.labels[self.labels["used"] < self.labels["k"]]

        if self.training:
            if df.shape[0] > 0:
                df = df.sample(n=min(int(self.batch_size * self.positive_prop), df.shape[0]))
            else:
                df = self.labels.sample(n=int(self.batch_size * self.positive_prop))
        else:
            df = self.labels.iloc[slice(index*self.batch_size, min((index+1)*self.batch_size, self.labels.shape[0]))]

        starts = df["start"]
        ends = df["end"]
        names = list(df["name"])
        channels = df['channel']

        if len(names) < self.batch_size:
            try:
                neg_batch = self.negative_samples.sample(n=self.batch_size - len(names))
            except AttributeError:
                neg_batch = self.labels.sample(n=self.batch_size - len(names))
            names.extend(neg_batch["name"].tolist())
            neg_starts = (neg_batch["start"] + np.random.random(neg_batch.shape[0]) * (neg_batch["end"] - neg_batch["start"] - self.duration))
            #neg_ends = neg_starts + self.n_times / self.fps
            neg_ends = neg_starts + duration
            starts = np.concatenate([starts, neg_starts.to_list()])
            ends = np.concatenate([ends, neg_ends.to_list()])
            channels = np.concatenate([channels, np.random.randint(0, self.max_channels, size=self.batch_size - len(names))])

        return names, starts, ends, channels

    def label_gen(self, name, start, end):
        """
        Label generator function for the generator.

        Returns an array of shape (number of classes, 1) per frame.
        NB: col and classes are passed from BaseAudioGenerator.

        :param ignore: Classes to ignore for training. Will become masked ?
        :param labels: Pandas dataframe containing the original data
        :param start: start of the chunk for which we are generating the label
        :param dur: duration of the chunk for which we are generating the label. If set to below 0; use the entire file.
        :param frames: number of frames in the final label (i.e number of time steps in an audio chunk)

        :return: an array of 0 and 1s, of shape (frames, ) if classes=None or exclusive_classes=True, else
        (len(classes), frames)
        """
        label_df = self.labels.loc[(self.labels.name == name) &
                                   (self.labels.end > start) &
                                   (self.labels.start <= end), :].copy()
        self.labels.loc[label_df.index, "used"] += 1
        labels = 1. if label_df.shape[0] > 0 else 0.
        mask = None
        return labels, mask, label_df


class AudioGenerator(TrainingGenerator):
    """
    Generator specifically for validation and inference.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(training=False, *args, **kwargs)
        if isinstance(self.duration, list):
            raise ValueError("Duration should be a single number (float or int) for validation.")


if __name__ == "__main__":
    if "Soline" in config.config_path:
        os.chdir("C:/Users/Faulst/Documents/Soline/Code/database_rooks/data")
    elif "killian" in config.config_path:
        os.chdir("C:/Users/killian/Desktop/database_rooks")
    data = "data"
    # path = "runs"
    training = "training"
    validation = "validation"
    test = "test"
    test = TrainingGenerator(data=validation,
                  batch_size=8,
                  wl=0.05,  # window length in seconds
                  window=tf.signal.hamming_window,
                  n_mels=128,  # number of mel coefficients in a frame of the spectrogram
                  n_times=128,
                  dtype="float32",  # default
                  audio_format="wav",  # default
                  labels_format="tsv",  # default
                  augment_function=None,
                  n_channels=6,
                  duration=1,  # durations in seconds
                  ovlp=75,  # overlap between successive windows
                  negative_prop=0,
                  phase=False,
                  scale=False,
                  norm=True,
                  test=True,
                  random_channel=True,
                  )
    print(test.__len__())
    chunks, y, masks = test.__getitem__(0)
