import os
import glob
import importlib.util
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import StrMethodFormatter
import librosa.display as disp
from tensorflow.python.autograph.pyct.origin_info import Location
from audio_generator import AudioGenerator
from network import *
from mel_to_audio import mel_to_stft_librosa, stft_to_wav_GL, write_audio
from mpl_toolkits.axes_grid1 import Grid


def interp(model, batch, coef, phase):
    if isinstance(coef, float):
        coef = [coef]
    if batch.shape[0] == len(coef) + 1:
        coeff = [*coef, 1-sum(coef)]
    elif batch.shape[0 == len(coef)]:
        coeff = coef / sum(coef)
    else:
        raise ValueError(f"coef should be of length either {batch.shape[0] - 1} (if given as list of float between 0 and 1) \
                        or {batch.shape[0]} (if given as list of float above 0), but was length {len (coef)}." )
    coeff = np.array(coeff)[np.newaxis, ...]
    if phase == 'shared':
        batch, _ = tf.unstack(batch, axis=-2)
        z = model.enc_mag(batch)
        z = np.matmul(coeff, z)
        xg = tf.squeeze(model.dec_mag(z))
    else:
        z = model.enc(batch)
        z = np.matmul(coeff, z)
        xg = tf.squeeze(model.dec(z))
    return xg


def multiple_interp(model, batch, steps, phase):
    coef = np.linspace(0, 1, steps+2)
    originals = tf.squeeze(model(batch)[1])
    if phase:
        originals, _ = tf.unstack(originals, axis=-1)
    return coef, [originals[1], *[interp(model, batch, c, phase=phase) for c in coef[1:-1]], originals[0]]


def get_interp_and_coef(model_name:str, voc_path:str, batch, steps):
    # model(s) path(s)
    script_path = os.path.realpath(__file__)
    if "Soline" in script_path:
        os.chdir("C:/Users/Faulst/Documents/Soline/Code/database_rooks")
    elif "killian" in script_path:
        os.chdir("C:/Users/killian/Desktop/database_rooks")
    # pass a list of paths to get multiple models
    models = "runs/GAIA"

    # Retrieve model
    model_path = f"{models}/{model_name}"
    weights_path = glob.glob(f"{model_path}/*.h5")

    # Retrieve config
    spec = importlib.util.spec_from_file_location("config", f'{model_path}/config.py')
    config = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(config)

    # Generator
    config.args.update({"batch_size": 2})
    config.args.update({"random_channel": False})
    config.args.update({"test": False})
    # obtain data
    path = f"data/interpolation/{voc_path}"  
    data = os.listdir(path)
    test_dataset = AudioGenerator(data=path,
                                **config.args,
                                )

    # Interpolate function
    if model_name == '20210608_001004' or model_name == '20210611_180553':
        interpolate_z = interpolate_z_VAE
    else:
        interpolate_z = interpolate_z_GAIA

    # Model
    if hasattr(config, "using_phase") and config.using_phase == "shared":
        gaia_kwargs = dict(
            enc_input_mag = tf.keras.layers.Input(shape=config.DIMS),
            enc_input_phase = tf.keras.layers.Input(shape=config.DIMS),
            dec_out_mag = tf.keras.layers.Conv2DTranspose(filters=1, kernel_size=3, strides=(1, 1), padding="SAME", activation=config.final_dec_activation[0]),
            dec_out_phase = tf.keras.layers.Conv2DTranspose(filters=1, kernel_size=3, strides=(1, 1), padding="SAME", activation=config.final_dec_activation[1]),
            decDIMS=config.DEC_DIMS,
            N_Z = config.N_Z,
            activation=config.activation,
            batchnorm=config.batchnorm,
            final_activation=config.final_dec_activation
        )
    else:
        gaia_kwargs = dict(
            enc=encoder,
            encDIMS=config.DIMS,
            N_Z=config.N_Z,
            BATCH_SIZE=config.BATCH_SIZE,
            activation=config.activation,
            batchnorm=config.batchnorm,
            dec=decoder,
            decDIMS=config.DEC_DIMS,
            final_activation=config.final_dec_activation
        )

    model = GAIA(
        using_phase=config.using_phase if hasattr(config, "using_phase") else False,
        **gaia_kwargs,
        unet_list=unet_function(config.DIMS,
                                config.BATCH_SIZE,
                                activation=config.activation,
                                final_activation=config.final_disc_activation),
        gen_optimizer=config.gen_optimizer,
        disc_optimizer=config.disc_optimizer,
        interpolate_function = interpolate_z,
        interpolate_kwargs=dict(chsq=Chi2(df=1 / config.args["batch_size"]))
    )

    # Model initialisation and loading
    model(test_dataset.__getitem__(0)[0])
    model.load_weights(weights_path[-1])

    # Get coef and interps
    batch = test_dataset.__getitem__(0)[0]
    coef, interps = multiple_interp(model=model, batch=batch, steps=steps, phase=config.using_phase)
    return coef, interps, batch


def get_n_interp_and_coef(model_list, voc_path, batch, steps):
    coef_list, interp_list, batch_list = zip(*[get_interp_and_coef(model_name, voc_path, batch, steps) for model_name in model_list])
    coefs = coef_list[0]
    batch = batch_list[0]
    return interp_list, coefs, batch


def plot_n_interp(coefs, interp_list, batch, model_title, voc_path):

    fig, ax = plt.subplots(nrows=len(interp_list), ncols=len(coefs)+3, sharex=True, sharey=True)
    for col in range(len(coefs)):
        for i in range(len(interp_list)):
            cax = disp.specshow((interp_list[i][col]).numpy(), sr=48000, hop_length=287, ax=ax[i,col+1], x_axis="time", y_axis="mel", vmin=0, vmax=1)
            ax[i,col+1].set_xlabel("Temps (s)", size=12)
            ax[i,col+1].set_ylabel("Fréquence (Hz)", size=12)
            ax[i,col+1].tick_params(labelsize=11)
            ax[i,col+1].label_outer()
        ax[0, col+1].set_title(f'{100-int(coefs[col]*100)}%\n{int(coefs[col]*100)}%')
        
    batch = tf.squeeze(batch)
    voc2, voc1 = tf.unstack(batch, axis=0)
    for i in range(len(interp_list)):
        # Voc1
        cax = disp.specshow(voc1.numpy(), sr=48000, hop_length=287, ax=ax[i,0], x_axis="time", y_axis="mel", vmin=0, vmax=1)
        ax[i,0].set_xlabel("Temps (s)", size=12)
        ax[i,0].set_ylabel("Fréquence (Hz)", size=12)
        ax[i,0].tick_params(labelsize=11)
        ax[i,0].label_outer()
        ax[0, 0].set_title("Individu 1 (original)\n")
        # Voc2
        cax = disp.specshow(voc2.numpy(), sr=48000, hop_length=287, ax=ax[i,len(coefs)+1], x_axis="time", y_axis="mel", vmin=0, vmax=1)
        ax[i,len(coefs)+1].set_xlabel("Temps (s)", size=12)
        ax[i,len(coefs)+1].set_ylabel("Fréquence (Hz)", size=12)
        ax[i,len(coefs)+1].tick_params(labelsize=11)
        ax[i,len(coefs)+1].label_outer()
        ax[0, len(coefs)+1].set_title("\nIndividu 2 (original)")

    for i in range(1,len(interp_list)+1):
        cax = disp.specshow((interp_list[0][0]).numpy(), sr=48000, hop_length=287, ax=ax[len(interp_list)-1, len(coefs)+2], x_axis="time", y_axis="mel", vmin=0, vmax=1)
        fig.colorbar(cax, ax=ax[len(interp_list)-i, len(coefs)+2], location='left').ax.yaxis.set_ticks_position("right")
        fig.delaxes(ax[len(interp_list)-i, len(coefs)+2])

    # fig.tight_layout()
    plt.setp(ax, xticklabels=[' ', 0.1, ' ', 0.3 ,' ' ,0.5 , ' ', 0.7])
    plt.subplots_adjust(wspace=0.02, hspace=0.05)
    plt.savefig(f"plot/{model_title}") 
    plt.show()


if __name__ == "__main__":
    model_list = ['20210527_191236', '20210611_180553']
    model_title = ['GAIA', 'VAE-GAN']
    voc_path = 'BalboBrain'
    interp_list, coefs, batch = get_n_interp_and_coef(model_list, voc_path, batch=2, steps=4)
    for model_name, model_title, interp in zip(model_list, model_title, interp_list):
        stfts = mel_to_stft_librosa(interp, wl=1440)
        stfts = tf.convert_to_tensor(stfts)
        wavs = stft_to_wav_GL(stfts, step=287, wl=1440)
        write_audio(model_name, wavs, origin=f'{voc_path}/{model_title}_interp')
    plot_n_interp(coefs, interp_list, batch, model_title=model_title, voc_path=voc_path)
