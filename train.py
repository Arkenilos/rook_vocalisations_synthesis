import tensorflow as tf
import numpy as np
import pandas as pd
from tensorflow_probability.python.distributions import Chi2
from IPython import display
import datetime
import csv
from shutil import copy
from audio_generator import *
from network import *
import config

# datasets
if "Soline" in os.path.realpath(__file__):
    os.chdir("C:/Users/cog/Documents/Soline/Code/database_rooks")
elif "killian" in os.path.realpath(__file__):
    os.chdir("C:/Users/killian/Desktop/database_rooks")
data = "data"
path = "runs"
training = os.path.join(data, "training")
validation = os.path.join(data, "validation")
test = os.path.join(data, "test")

train_dataset = TrainingGenerator(data=training,
                                  **config.args,
                                  **config.training_args,
                                  )

test_dataset = AudioGenerator(data=validation,
                              **config.args
                              )


def __main__(train_type="lr"):
    import config

    # pandas dataframe to save the loss information to
    losses = pd.DataFrame(columns=["X_D_G_X_loss",
                                   "X_D_G_Zi_loss",
                                   "X_G_loss",
                                   "X_D_X_loss",
                                   "X_G_X_loss",
                                   "G_loss",
                                   "D_loss"])

    # model
    if config.using_phase == "shared":
        gaia_kwargs = dict(
            enc_input_mag = tf.keras.layers.Input(shape=config.DIMS),
            enc_input_phase = tf.keras.layers.Input(shape=config.DIMS),
            dec_out_mag = tf.keras.layers.Conv2DTranspose(filters=1, kernel_size=3, strides=(1, 1), padding="SAME", activation=config.final_dec_activation[0]),
            dec_out_phase = tf.keras.layers.Conv2DTranspose(filters=1, kernel_size=3, strides=(1, 1), padding="SAME", activation=config.final_dec_activation[1]),
            decDIMS=config.DEC_DIMS,
            N_Z=config.N_Z,
            activation=config.activation,
            batchnorm=config.batchnorm,
            final_activation=config.final_dec_activation
        )
    else:
        gaia_kwargs = dict(
            enc=encoder,
            encDIMS=config.DIMS,
            N_Z=config.N_Z,
            BATCH_SIZE=config.BATCH_SIZE,
            activation=config.activation,
            batchnorm=config.batchnorm,
            dec=decoder,
            decDIMS=config.DEC_DIMS,
            final_activation=config.final_dec_activation
        )

    model = GAIA(
        using_phase=config.using_phase,
        **gaia_kwargs,
        unet_list=unet_function(config.DIMS,
                                config.BATCH_SIZE,
                                activation=config.activation,
                                final_activation=config.final_disc_activation),
        gen_optimizer=config.gen_optimizer,
        disc_optimizer=config.disc_optimizer,
        interpolate_function = interpolate_z_GAIA,
        interpolate_kwargs=dict(chsq=Chi2(df=1 / config.args["batch_size"]))
    )

    now = datetime.datetime.now().strftime('%Y%m%d_%H%M%S')

    if train_type == 'lr':
        lr_min = 1e-8
        lr_max = 1.0
        n_epochs = 10
        
        copy(config.config_path, f"lr_test/{now}_config.py")
        
        with open(f"lr_test/{now}.csv", 'w', newline='') as f:
            writer = csv.writer(f)
            if config.using_phase == "separate":
                writer.writerow(["epoch", "batch", "lr", "G_mag_loss", "G_phase_loss", "D_mag_loss", "D_phase_loss"])
            else:
                writer.writerow(["epoch", "batch", "lr", "G_loss", "D_loss"])
        
        for epoch in range(n_epochs):
            # train
            loss = []
            for c in range(train_dataset.__len__()):
                t = (epoch * train_dataset.__len__() + c)/(n_epochs * train_dataset.__len__())
                lr = lr_min * (lr_max/lr_min) ** t
                if config.using_phase == "separate":
                    for optim in [model.mag_disc_optimizer,
                                  model.mag_gen_optimizer,
                                  model.phase_disc_optimizer,
                                  model.phase_gen_optimizer]:
                        K.set_value(optim.lr, K.get_value(lr))
                else:
                    K.set_value(model.gen_optimizer.lr, K.get_value(lr))
                    K.set_value(model.disc_optimizer.lr, K.get_value(lr))

                chunks, y, masks = train_dataset.__getitem__(c)
                model.train_net(chunks, y)
                loss = model.compute_loss(chunks, y)[-2:]
                if config.using_phase == "separate":
                    loss = tf.reshape(loss, -1).numpy()
                else:
                    loss = np.array(loss)
                print(loss)
        
                with open(f'lr_test/{now}.csv', 'a', newline='') as f:
                    writer = csv.writer(f)
                    writer.writerow([epoch, c, lr, *loss])
                print(c)
        
            train_dataset.on_epoch_end()
            # plot results
            display.clear_output()
            print(
                "Epoch: {}".format(epoch)
            )
    
    else:
        loss_min = [10., 10.]
        epochs_since_last_min = 0

        model(train_dataset.__getitem__(0)[0])

        os.makedirs(f"runs/GAIA/{now}")
        copy(config.config_path, f"runs/GAIA/{now}")
        with open(f"runs/GAIA/{now}/validation_loss.csv", 'w', newline='') as f:
            writer = csv.writer(f)
            if config.using_phase == "separate":
                writer.writerow(["epoch", "G_mag_loss", "G_phase_loss", "D_mag_loss", "D_phase_loss", "val_G_mag_loss", "val_G_phase_loss", "val_D_mag_loss", "val_D_phase_loss"])
            else:
                writer.writerow(["epoch", "G_loss", "D_loss", "val_G_loss", "val_D_loss"])

        for epoch in range(config.n_epochs):
            # train
            loss = []
            for c in range(train_dataset.__len__()):
                chunks, y, masks = train_dataset.__getitem__(c)
                model.train_net(chunks, y)
                losses = model.compute_loss(chunks, y)[-2:]
                loss.append(losses)

            # test on holdout
            val_loss = []
            for c in range(test_dataset.__len__()):
                chunks, y, _ = test_dataset.__getitem__(c)
                val_loss.append(model.compute_loss(chunks, y)[-2:])

            if config.using_phase == "separate":
                loss = tf.stack([tf.stack([tf.unstack(l) for l in _]) for _ in loss])
                val_loss = tf.stack([tf.stack([tf.unstack(l) for l in _]) for _ in val_loss])
                epoch_loss = np.mean(loss, axis=0)
                val_epoch_loss = np.mean(val_loss, axis=0)
                epoch_loss = tf.reshape(epoch_loss, -1).numpy()
                val_epoch_loss = tf.reshape(val_epoch_loss, -1).numpy()

            else:
                epoch_loss = np.mean(loss, axis=0)
                val_epoch_loss = np.mean(val_loss, axis=0)

            with open(f"runs/GAIA/{now}/validation_loss.csv", 'a', newline='') as f:
                writer = csv.writer(f)
                writer.writerow([epoch, *epoch_loss, *val_epoch_loss])

            if (config.using_phase == "separate" and (val_epoch_loss[0] < loss_min[0]) or val_epoch_loss[1] < loss_min[1]) or \
               (config.using_phase != "separate" and val_epoch_loss[0] < loss_min[0]):
                loss_min = val_epoch_loss
                model.save_weights(filepath=f"runs/GAIA/{now}/ep{epoch:05d}.h5")
                epochs_since_last_min = 0
            else:
                epochs_since_last_min += 1
            if epochs_since_last_min > config.epoch_threshold:
                break

            train_dataset.on_epoch_end()
            test_dataset.on_epoch_end()

            # plot results
            display.clear_output()
            print(
                "Epoch: {}".format(epoch)
            )

def send_email(subject="Fini",
               body="Entrainement fini!"):
    import smtplib
    server = smtplib.SMTP(host='HOST', port=587)
    server.ehlo()
    server.starttls()

    server.login('LOGIN', 'PASSWORD')

    message = f'Subject: {subject}\n\n{body}'

    server.sendmail('MAIL_ADRESS', 'MAIL_ADRESS', message)
    server.quit()


if __name__ == "__main__":
    train_type = "lr"
    __main__(train_type=train_type)
    send_email()
